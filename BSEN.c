#define _USE_MATH_DEFINES

#include "BSEN.h"
#include "stb_image.h"
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdio.h>

//#define CANVAS_ACCESS(_MARG_X, _MARG_Y, _MARG_W, _MARG_H) ( (_MARG_X) + ((_MARG_W) * (_MARG_Y)) )
#define CANVAS_ACCESS(_MARG_X, _MARG_Y, _MARG_W, _MARG_H) ( (_MARG_X) + ((_MARG_W) * (_MARG_Y)) )


//temp as hell
#include "BSEN_app.h"

//access with BSEN_FOV_<...>
static const float _BSEN_HALF_FOV[3] = {
    ( M_PI / 4.0),
    ( M_PI * 63.434948822922 / 180.0),
    ( M_PI * 75.963756532074 / 180.0)
};

//access with BSEN_FOV_<...>
static const int _BSEN_FOV_DIVISION_FACTOR[3] = {
    1,2,4
};

static inline int iclamp(int v, int min, int max) {
	if (v < min) return min;
	if (v > max) return max;
	return v;
}

static inline int fclamp(float v, float min, float max) {
	if (v < min) return min;
	if (v > max) return max;
	return v;
}

static inline int fsign(float v) {
	if (v < 0) return -1;
	if (v > 0) return 1;
	return 0;
}

static inline int isign(int v) {
	if (v < 0) return -1;
	if (v > 0) return 1;
	return 0;
}

static inline float normalize_angle(float theta) {
    const float _2PI = 2.0 * M_PI;
    while ( theta < 0 )    { theta += _2PI; }
    while ( theta > _2PI ) { theta -= _2PI; }
    return theta;
}

static inline float angle_difference(float a1, float a2) {
    return M_PI - fabs(fabs(a1 - a2) - M_PI); //thanks random person in gamedev stack exchange
}

static void _BSEN_slice_clear_color(BSEN_rendering_slice* slice, int w) {
    memset( slice->pixel_buffer, 0, w * slice->h * sizeof(BSEN_bytecolor) );
}

static void _BSEN_slice_clear_flood(BSEN_rendering_slice* slice, int w) {
    memset( slice->flood_buffer, BSEN_FLOOD_EMPTY, w * slice->h * sizeof(uint8_t) );
}

static void _BSEN_slice_clear_depth(BSEN_rendering_slice* slice, int w) {

    for (int py = 0; py < slice->h; py++)
    for (int px = 0; px < w; px++) {
        slice->depth_buffer[CANVAS_ACCESS(px, py, w, slice->h) ] = BSEN_Z_FAR;
    }
}

//px/py/flashlight will be implemented later
static inline uint8_t _BSEN_calculate_intensity(uint8_t intensity, float depth,
float diminished_range, float directional_light_factor, int px, int py, int apply_flashlight ) {

    float light = fmax( diminished_range - depth, 0.0 ) / diminished_range;
    light *= directional_light_factor;
    intensity *= light;
    return intensity;
}

void BSEN_context_set_palette(BSEN_context* ctx, BSEN_rgb groups[]) {

    for (uint8_t cg = 0; cg < BSEN_COLOR_GROUPS; cg++)
    for (uint8_t intensity = 0; intensity < BSEN_INTENSITY_LEVELS; intensity++) {
        BSEN_rgb color;
        float br = ((float)intensity) / (float)(BSEN_INTENSITY_LEVELS - 1);
        color.r = (uint8_t)( groups[cg].r * br );
        color.g = (uint8_t)( groups[cg].g * br );
        color.b = (uint8_t)( groups[cg].b * br );

        uint8_t index = (cg << BSEN_COLOR_GROUP_SHIFT) | ( intensity & BSEN_INTENSITY_MASK );

        ctx->color_palette[index] = color;
    }
}

void BSEN_context_camera_pos(BSEN_context* ctx, float x, float y, float z) {

    ctx->camera.pos.x = x;
    ctx->camera.pos.y = y;
    ctx->camera.pos.z = z;
}

void BSEN_context_camera_angle(BSEN_context* ctx, float hangle, float yshear) {

    ctx->camera.hangle = hangle;
    ctx->camera.yshear = yshear;
}

void BSEN_context_camera_FOV(BSEN_context* ctx, uint8_t FOV) {

    ctx->camera.FOV = FOV;
}

void BSEN_context_clear_color(BSEN_context* ctx) {

    memset( ctx->pixel_buffer, 0, ctx->w * ctx->h * sizeof(BSEN_bytecolor) );
}

void BSEN_context_clear_flood(BSEN_context* ctx) {

    memset( ctx->flood_buffer, BSEN_FLOOD_EMPTY, ctx->w * ctx->h * sizeof(uint8_t) );
}

void BSEN_context_clear_depth(BSEN_context* ctx) {

    for (int py = 0; py < ctx->h; py++)
    for (int px = 0; px < ctx->w; px++) {
        ctx->depth_buffer[CANVAS_ACCESS(px, py, ctx->w, ctx->h) ] = BSEN_Z_FAR;
    }
}

static uint8_t _BSEN_find_closest_bytecolor( BSEN_context* ctx, int r, int g, int b ) {

    int closest_index = 0;
    int closest_difference = 9999;

    //don't go through 255 since that color is considered to be alpha = 0
    for (int bc = 0; bc <= 254; bc++) {
        int pr, pg, pb;
        pr = (int)ctx->color_palette[bc].r;
        pg = (int)ctx->color_palette[bc].g;
        pb = (int)ctx->color_palette[bc].b;

        int diff = abs(pr - r) + abs(pg - g) + abs(pb - b);

        if (diff < closest_difference) {
            closest_index = bc;
            closest_difference = diff;
        }
    }

    return (uint8_t)closest_index;
}

BSEN_texture* BSEN_texture_load_png(BSEN_context* ctx, const char* filename, int rotate) {

	int w, h, n;
	uint8_t* data = stbi_load(filename, &w, &h, &n, 4);
	if (data != NULL) {
		BSEN_texture* new_tx = malloc(sizeof(BSEN_texture));
		new_tx->w = w;
		new_tx->h = h;
		new_tx->data = malloc( sizeof(BSEN_bytecolor) * w * h );

		if (!rotate) {

            for (int yy = 0; yy < h; yy++)
            for (int xx = 0; xx < w; xx++) {
                int r,g,b,a;
                r = (int)data[4*(xx + w*yy)];
                g = (int)data[4*(xx + w*yy) + 1];
                b = (int)data[4*(xx + w*yy) + 2];
                a = (int)data[4*(xx + w*yy) + 3];

                if (a <= 10) {
                    new_tx->data[xx + w*yy] = BSEN_COLOR_ALPHA_KEY;
                    continue;
                }

                new_tx->data[xx + w*yy] = _BSEN_find_closest_bytecolor( ctx, r, g, b );
            }

		}
		else {
            new_tx->w = h;
            new_tx->h = w;


            int pid = 0;
            for (int xx = 0;     xx <  w; xx++)
            for (int yy = h - 1; yy >= 0; yy--) {

                int r,g,b,a;
                r = (int)data[4*(xx + w*yy)];
                g = (int)data[4*(xx + w*yy) + 1];
                b = (int)data[4*(xx + w*yy) + 2];
                a = (int)data[4*(xx + w*yy) + 3];

                BSEN_bytecolor c;

                if (a <= 10) {
                    c = BSEN_COLOR_ALPHA_KEY;
                }
                else {
                    c = _BSEN_find_closest_bytecolor( ctx, r, g, b );
                }

                new_tx->data[ pid ] = c;

                pid++;
            }
		}

		stbi_image_free(data);
		return new_tx;
	}
	else {
		return NULL;
	}
}


//file format for this will be the output from goxel's "text" option. Example:
/*
# Goxel 0.12.0
# One line per voxel
# X Y Z RRGGBB
-4 -16 0 8f563b
-3 -16 0 8f563b
-2 -16 0 8f563b
-1 -16 0 8f563b
(more voxels here...)
*/
BSEN_model* BSEN_model_load_text (BSEN_context* ctx, const char* filename) {

    FILE* f = fopen(filename, "r");

    if (f) {
        const int str_size = 100;
        char line_str[ str_size ];

        int x,y,z;
        int r,g,b;

        int min_x, max_x, min_y, max_y, min_z, max_z;
        int initialized_ranges = 0;

        int nvoxels = 0;
        BSEN_voxel_in_file* voxel_array = NULL;

        while ( fgets( line_str, str_size, f) ) {

            int scan_value = sscanf( line_str, "%d %d %d %2x%2x%2x\n", &x, &y, &z, &r, &g, &b );
            if ( scan_value == 6 ) {

                z = -z;
                y = -y;

                if ( voxel_array == NULL ) {
                    voxel_array = malloc( 1 * sizeof (BSEN_voxel_in_file) );
                }
                else {
                    voxel_array = realloc( voxel_array, ( nvoxels + 1 ) * sizeof (BSEN_voxel_in_file) );
                }

                voxel_array[ nvoxels ] = (BSEN_voxel_in_file) {
                    .x = x, .y = y, .z = z,
                    .c = _BSEN_find_closest_bytecolor( ctx, r, g, b )
                };

                if ( !initialized_ranges ) {
                    min_x = x;
                    max_x = x;
                    min_y = y;
                    max_y = y;
                    min_z = z;
                    max_z = z;

                    initialized_ranges = 1;
                }
                else {
                    if (x < min_x) { min_x = x; }
                    if (x > max_x) { max_x = x; }
                    if (y < min_y) { min_y = y; }
                    if (y > max_y) { max_y = y; }
                    if (z < min_z) { min_z = z; }
                    if (z > max_z) { max_z = z; }

                }

                nvoxels++;
            }
        }

        fclose(f);

        if (nvoxels > 0) {

            BSEN_model* model = malloc( sizeof( BSEN_model ) );
            model->xoffset = -min_x;
            model->yoffset = -min_y;
            model->xsize = 1 + (max_x - min_x);
            model->ysize = 1 + (max_y - min_y);
            //model->zsize = 1 + (max_z - min_z);
            model->zmax = max_z;
            model->zmin = min_z;

            model->va_grid = malloc( model->xsize * model->ysize * sizeof (BSEN_voxel_array) );
            for (int py = 0; py < model->ysize; py++)
            for (int px = 0; px < model->xsize; px++) {
                model->va_grid[px + model->xsize * py].nvoxels = 0;
                model->va_grid[px + model->xsize * py].varray = NULL;
            }


            int xoffset = model->xoffset;
            int yoffset = model->yoffset;

            for (int py = 0; py < model->ysize; py++)
            for (int px = 0; px < model->xsize; px++)
            for (int  v = 0; v  < nvoxels; v++) {
                if ( px == (voxel_array[v].x + xoffset) && py == (voxel_array[v].y + yoffset) ) {

                    BSEN_voxel_array* va_ref = &model->va_grid[px + model->xsize * py];

                    if ( va_ref->varray == NULL ) {
                        va_ref->varray = malloc( 1 * sizeof( BSEN_voxel_array_entry ) );
                    }
                    else {
                        va_ref->varray = realloc( va_ref->varray, (va_ref->nvoxels + 1) * sizeof( BSEN_voxel_array_entry ) );
                    }

                    va_ref->varray[ va_ref->nvoxels ] =
                        (BSEN_voxel_array_entry) { .z = voxel_array[v].z, .c = voxel_array[v].c };

                    va_ref->nvoxels++;
                }
            }

            free(voxel_array);

            return model;
        }
    }

    return NULL;
}

void BSEN_model_free( BSEN_model* m ) {

    for (int py = 0; py < m->ysize; py++)
    for (int px = 0; px < m->xsize; px++) {
        if ( m->va_grid[px + m->xsize * py].varray ) { free( m->va_grid[px + m->xsize * py].varray ); }
    }

    free(m->va_grid);
    free(m);
}

void BSEN_context_draw_texture(BSEN_context* ctx, int x, int y, BSEN_texture* tex) {

    int w = ctx->w;
    int h = ctx->h;

    for (int ty = 0; ty < tex->h; ty++)
    for (int tx = 0; tx < tex->w; tx++) {

        int px, py;
        px = x + tx;
        py = y + ty;

        if (x < 0 || x >= w || y < 0 || y >= h) { continue; }

        ctx->pixel_buffer[ CANVAS_ACCESS(px, py, w, h) ] = tex->data[tx + tex->w * ty];
    }

}

static inline float _BSEN_lerp_dest_ptg(float left, float mid, float right) {

    mid -= left;
    right -= left;
    //left = 0;

    if (right == 0) return 0;

    return mid / right;
}

static inline float _BSEN_float2_magnitude(float x, float y) {
    return sqrt( (x * x) + (y * y) );
}

static inline float _BSEN_float3_magnitude(float x, float y, float z) {
    return sqrt( (x * x) + (y * y) + (z * z) );
}

static inline float _BSEN_float2_distance(float x1, float y1, float x2, float y2) {
    x1 -= x2;
    y1 -= y2;

    return sqrt( x1*x1 + y1*y1 );
}

//stolen from jdh's boomer shooter thingy
static inline BSEN_float2 _BSEN_intersect_segs(BSEN_float2 a0, BSEN_float2 a1, BSEN_float2 b0, BSEN_float2 b1) {
    float d =
        ((a0.x - a1.x) * (b0.y - b1.y))
            - ((a0.y - a1.y) * (b0.x - b1.x));

    if (fabsf(d) < 0.000001f) { return (BSEN_float2) { NAN, NAN }; }

    float
        t = (((a0.x - b0.x) * (b0.y - b1.y))
                - ((a0.y - b0.y) * (b0.x - b1.x))) / d,
        u = (((a0.x - b0.x) * (a0.y - a1.y))
                - ((a0.y - b0.y) * (a0.x - a1.x))) / d;
    return (t >= 0 && t <= 1 && u >= 0 && u <= 1) ?
        ((BSEN_float2) {
            a0.x + (t * (a1.x - a0.x)),
            a0.y + (t * (a1.y - a0.y)) })
        : ((BSEN_float2) { NAN, NAN });
}

//rotate vec2 around (0,0)
//if the point is somehing like (x=+value, y=0),
//small positive theta values will move the point towards positive y values
//x,y values passed as reference since we probably will need to rotate
//different pairs from a vec3 (like first an x,y rotation, then x,z)
static inline void _BSEN_float2_rotate(float* x, float* y, float theta) {
	float xp, yp;
	xp = (*x) * cos(theta) - (*y) * sin(theta);
	yp = (*x) * sin(theta) + (*y) * cos(theta);
	*x = xp;
	*y = yp;
}

void BSEN_context_temp_push_wall(BSEN_context* ctx, BSEN_wall* wall) {
    ctx->temp_scene.walls[ctx->temp_scene.nwalls] = wall;
    ctx->temp_scene.nwalls++;
}

void BSEN_context_temp_push_flat(BSEN_context* ctx, BSEN_flat* flat) {
    ctx->temp_scene.flats[ctx->temp_scene.nflats] = flat;
    ctx->temp_scene.nflats++;
}

void BSEN_context_temp_push_model(BSEN_context* ctx, BSEN_model_instance* model) {
    ctx->temp_scene.models[ctx->temp_scene.nmodels] = model;
    ctx->temp_scene.nmodels++;
}

void BSEN_context_temp_push_sprite(BSEN_context* ctx, BSEN_3D_sprite spr) {
    ctx->temp_scene.sprites[ctx->temp_scene.nsprites] = spr;
    ctx->temp_scene.nsprites++;
}

static inline void _BSEN_slice_line_znear(BSEN_context* ctx, BSEN_float2_line* line, int outside_id,
float* wall_ytex) {

    int inside_id = 1 - outside_id;
    float xdist = line->point[inside_id].x - line->point[outside_id].x;
    float in_length = -line->point[outside_id].x;
    float in_ptg = in_length / xdist;
    float out_ptg = 1.0 - in_ptg;

    line->point[outside_id].x = BSEN_Z_NEAR;
    line->point[outside_id].y = (line->point[outside_id].y * out_ptg) + ( line->point[inside_id].y * in_ptg);
    if ( wall_ytex == NULL ) { return; }
    wall_ytex[outside_id] = (wall_ytex[outside_id] * out_ptg) + (wall_ytex[inside_id] * in_ptg);
}

//returns 0 if both points are outside, 1 if no slice, 2 if sliced
static inline int _BSEN_check_and_slice_line_znear( BSEN_context* ctx, BSEN_float2_line* line,
float* wall_ytex ) {

    //both points behind z near
    if ( line->point[0].x < BSEN_Z_NEAR && line->point[1].x < BSEN_Z_NEAR ) { return 0; }

    //only one behind z near
    if ( line->point[0].x < BSEN_Z_NEAR || line->point[1].x < BSEN_Z_NEAR ) {

        int outside_id = ( line->point[0].x < BSEN_Z_NEAR ) ? 0 : 1;
        _BSEN_slice_line_znear(ctx, line, outside_id, wall_ytex);
        return 2;
    }

    return 1;
}

//assumes points are >= to znear, otherwise div by zero or some other wacky crazy shit could happen
static inline BSEN_float2 _BSEN_project_point(BSEN_context* ctx, const BSEN_float3* p, int yshear,
float w_half, float h_half ) {

    BSEN_float2 result;

    //from -1 to 1, where -1 is the leftmost pixel of the screen and 1 the rightmost
    float xscreen = (p->y / p->x) / _BSEN_FOV_DIVISION_FACTOR[ ctx->camera.FOV ];
    result.x = (xscreen) * w_half;
    result.x =  result.x + w_half;

    float yscreen = (p->z / p->x) / _BSEN_FOV_DIVISION_FACTOR[ BSEN_VFOV ];
    result.y = (yscreen) * h_half;
    result.y = result.y  + h_half;
    result.y += yshear;

    return result;
}

/*
static inline BSEN_int2 _BSEN_project_point(BSEN_context* ctx, const BSEN_float3* p, int yshear ) {

    BSEN_int2 result;

    int w = ctx->w;
    int h = ctx->h;

    //from -1 to 1, where -1 is the leftmost pixel of the screen and 1 the rightmost
    float xscreen = p->y / p->x;
    result.x = (xscreen) * (w >> 1); //aka /2
    result.x = result.x >> ctx->camera.FOV;
    result.x = result.x + (w >> 1);

    float yscreen = p->z / p->x;
    result.y = (yscreen) * (h >> 1);
    result.y = result.y >> BSEN_VFOV;
    result.y = result.y + (h >> 1);
    result.y += yshear;

    return result;
}
*/

//slice to the borders of the frustum
#define _SLICE_TO_FRUSTUM(marg_line, marg_angle_sign, marg_outside_operator, marg_wall_ytex_action, outside_action) \
{ \
    float angle = marg_angle_sign _BSEN_HALF_FOV[ctx->camera.FOV]; \
    _BSEN_float2_rotate( &marg_line.point[0].x, &marg_line.point[0].y, angle ); \
    _BSEN_float2_rotate( &marg_line.point[1].x, &marg_line.point[1].y, angle ); \
    \
    if ( marg_line.point[0].y marg_outside_operator 0 && marg_line.point[1].y marg_outside_operator 0 ) { outside_action } \
    \
    if ( marg_line.point[0].y marg_outside_operator 0 || marg_line.point[1].y marg_outside_operator 0 ) { \
        \
        int outside_id, inside_id; \
        \
        if ( marg_line.point[0].y marg_outside_operator 0 ) \
            outside_id = 0; \
        else \
            outside_id = 1; \
        \
        inside_id = 1 - outside_id; \
        \
        float ydist = fabs(marg_line.point[inside_id].y - marg_line.point[outside_id].y); \
        float in_length = fabs(marg_line.point[outside_id].y); \
        float in_ptg = in_length / ydist; \
        float out_ptg = 1.0 - in_ptg; \
        \
        marg_line.point[outside_id].y = 0; \
        marg_line.point[outside_id].x = (marg_line.point[outside_id].x * out_ptg) + (marg_line.point[inside_id].x * in_ptg); \
        marg_wall_ytex_action \
    } \
    \
    _BSEN_float2_rotate( &marg_line.point[0].x, &marg_line.point[0].y, -angle ); \
    _BSEN_float2_rotate( &marg_line.point[1].x, &marg_line.point[1].y, -angle ); \
    \
}


static void _BSEN_render_wall( BSEN_context* ctx, BSEN_rendering_slice* render_slice,
const BSEN_wall* wall, int yshear) {

    int w = ctx->w;
    int real_h = ctx->h;
    int h = render_slice->h;

    float w_half = w / 2;
    float h_half = real_h / 2;

    BSEN_float2_line line;
    memcpy( &line, &wall->line, sizeof(BSEN_float2_line) );

    line.point[0].x -= ctx->camera.pos.x;
    line.point[0].y -= ctx->camera.pos.y;
    line.point[1].x -= ctx->camera.pos.x;
    line.point[1].y -= ctx->camera.pos.y;

    float base_z = wall->base_z - ctx->camera.pos.z;

    float wall_ytex[2] = { wall->point_texture_y[0], wall->point_texture_y[1] };

    _BSEN_float2_rotate( &line.point[0].x, &line.point[0].y, ctx->camera.hangle );
    _BSEN_float2_rotate( &line.point[1].x, &line.point[1].y, ctx->camera.hangle );

    if ( !_BSEN_check_and_slice_line_znear( ctx, &line, wall_ytex ) ) { return; }

    /*

    _SLICE_TO_FRUSTUM( line, +, <,
        wall_ytex [outside_id]   = (wall_ytex [outside_id]   * out_ptg) + (wall_ytex [inside_id]   * in_ptg); ,
        return; )

    _SLICE_TO_FRUSTUM( line, -, >,
        wall_ytex [outside_id]   = (wall_ytex [outside_id]   * out_ptg) + (wall_ytex [inside_id]   * in_ptg); ,
        return; )

    */

    //assemble actual 3D coords
    BSEN_float3_line base, top;
    for (int a = 0; a < 2; a++) {
        base.point[a].x = line.point[a].x;
        base.point[a].y = line.point[a].y;
        base.point[a].z = base_z;

        top.point[a].x = line.point[a].x;
        top.point[a].y = line.point[a].y;
        top.point[a].z = base_z - wall->height; //-z is up
    }

    BSEN_float2_line base_sc, top_sc;
    for (int a = 0; a < 2; a++) {
        base_sc.point[a] = _BSEN_project_point( ctx, &base.point[a], yshear, w_half, h_half );
        top_sc.point [a] = _BSEN_project_point( ctx, &top.point [a], yshear, w_half, h_half );

        base_sc.point[a].y -= render_slice->yoffset;
        top_sc.point [a].y -= render_slice->yoffset;
    }

    //top_sc.point[0].x = base_sc.point[0].x;
    //top_sc.point[1].x = base_sc.point[1].x;

    int leftmost_id = 0;
    if ( base_sc.point[0].x > base_sc.point[1].x ) { leftmost_id = 1; }

    if (leftmost_id > 0 && !wall->draw_from_behind ) { return; }

    int rightmost_id = 1 - leftmost_id;

    float xspan = base_sc.point[rightmost_id].x - base_sc.point[leftmost_id].x;

    float ybase = base_sc.point[leftmost_id].y;
    float ybase_delta = (base_sc.point[rightmost_id].y - ybase) / (float)xspan;

    float ytop = top_sc.point[leftmost_id].y;
    float ytop_delta = (top_sc.point[rightmost_id].y - ytop) / (float)xspan;

    float xstart = base_sc.point[leftmost_id].x;

    float xend = xstart + xspan;
    if (xend >= w) {
        xend = w - 1;
    }

    if (xstart < 0) {
        ybase -= xstart * ybase_delta;
        ytop  -= xstart * ytop_delta;
        xstart = 0;
    }

    float diminished_range = ctx->light.diminished_range;
    float wall_angle = normalize_angle(
        atan2( wall->line.point[1].y - wall->line.point[0].y, wall->line.point[1].x - wall->line.point[0].x  ) );

    float wall_angle_ptg = angle_difference( wall_angle, ctx->light.directional_angle ) / M_PI;

    float directional_light_factor = ( wall_angle_ptg * ctx->light.directional_min ) + ( (1-wall_angle_ptg) * 1.0 );

    float far_x;
    if (line.point[1].x > line.point[0].x) { far_x = line.point[1].x; }
    else { far_x = line.point[0].x; }

    far_x += 1.0; //just in case, to guarantee collision

    float W = far_x * tan ( _BSEN_HALF_FOV[ctx->camera.FOV] );

    float wall_length = fmax( 0.01, _BSEN_float2_distance(line.point[0].x, line.point[0].y, line.point[1].x, line.point[1].y) );

    float ztop = base_z - wall->height;

    for (int px = xstart; px <= xend; px++) {

        BSEN_float2 camera_ray_target;
        camera_ray_target.y = W * (float)(px - w/2) / (float)(w/2);
        camera_ray_target.x = far_x;

        BSEN_float2 intersection = _BSEN_intersect_segs(line.point[0], line.point[1], camera_ray_target, (BSEN_float2){0,0} );

        if ( isnan(intersection.x) ) { continue; }

        float xy_depth2 = _BSEN_float2_magnitude(intersection.x, intersection.y);
        xy_depth2 *= xy_depth2;

        float dest_ptg = _BSEN_float2_distance(line.point[leftmost_id].x, line.point[leftmost_id].y, intersection.x, intersection.y);
        dest_ptg /= wall_length;

        float ytex  = ( dest_ptg * wall_ytex[rightmost_id] ) + ( (1.0 - dest_ptg) * wall_ytex[leftmost_id] );

        float yspan = fmax(0.01, ybase - ytop);

        float xtex = wall->top_texture_x;
        float xtex_delta = ( wall->base_texture_x - xtex ) / yspan;

        float ztop_depth  = sqrt( ztop   * ztop   + xy_depth2 );
        float zbase_depth = sqrt( base_z * base_z + xy_depth2 );

        float zapprox = base_z - wall->height;
        float zapprox_delta = ( base_z - zapprox ) / yspan;

        float ystart = (ytop);
        if (ystart < 0) {
            xtex -= roundf(ystart) * xtex_delta;
            zapprox -= roundf(ystart) * zapprox_delta;
            ystart = 0;
        }

        float yend = (ybase);
        if (yend >= h) {
            yend = h - 1;
        }

        int depth_scan_delay = 0;

        float depth;

        int yfor_start = roundf(ystart);
        int yfor_end   = roundf(yend);

        if (yfor_start <  0) yfor_start = 0;
        if (yfor_end   >= h) yfor_end   = h - 1;

        for (int py = yfor_start; py <= yfor_end; py++ ) {

            if ( depth_scan_delay <= 0 ) {
                depth = sqrt( zapprox*zapprox + xy_depth2);
                depth_scan_delay = ctx->depth_precision;
            }
            else { depth_scan_delay--; }

            int pix_id = CANVAS_ACCESS(px, py, w, h);

            if ( render_slice->depth_buffer[pix_id] > depth )
            {

                int tx = roundf(xtex);
                int ty = roundf(ytex);

                tx = abs(tx) % (wall->tx->w);
                ty = abs(ty) % (wall->tx->h);

                BSEN_bytecolor c = wall->tx->data[tx + wall->tx->w * ty];

                if (c != BSEN_COLOR_ALPHA_KEY) {

                    //float light = fmax( diminished_range - depth, 0.0 ) / diminished_range;

                    uint8_t intensity = c & BSEN_INTENSITY_MASK;
                    //intensity *= light;

                    intensity = _BSEN_calculate_intensity( intensity, depth, diminished_range, directional_light_factor,
                        px, py, 0);

                    c = (c & (~BSEN_INTENSITY_MASK)) | (intensity & BSEN_INTENSITY_MASK) ;

                    render_slice->pixel_buffer[pix_id] = c;
                    render_slice->depth_buffer[pix_id] = depth;
                }

            }

            xtex += xtex_delta;
            zapprox += zapprox_delta;
        }


        ybase += ybase_delta;
        ytop  += ytop_delta;

    }
}

//pass buffer as either ctx->pixel_buffer or ctx->flood_buffer
//pass the render_slice w+h
static void _BSEN_render_line( int w, int h, uint8_t* buffer, BSEN_bytecolor c,
float x1, float y1, float x2, float y2) {

    #define _INBOUNDS(marg_x, marg_y) \
    ( (marg_x) >= 0 && (marg_x) < w && (marg_y) >= 0 && (marg_y) < h )

    if (x1 == x2 && y1 == y2) {
        if ( _INBOUNDS((int)x1, (int)y1) )
            buffer[ CANVAS_ACCESS( (int)x1, (int)y1, w, h ) ] = c;
        return;
    }

    float xdist = x2 - x1;
    float ydist = y2 - y1;

    if (fabs(xdist) > fabs(ydist)) {
        float dy = ydist / fabs(xdist);
        int dx = fsign(xdist);
        int steps = fabs(xdist);

        float px = x1;
        float py = y1;

        while(steps >= 0) {
            if ( _INBOUNDS((int)px, (int)py) )
                buffer[ CANVAS_ACCESS( (int)px, (int)py, w, h ) ] = c;

            px += dx;
            py += dy;

            //if ( _OOB( (int)px, (int)py ) ) { return; }
            steps--;
        }
    }
    else {
        float dx = xdist / fabs(ydist);
        int dy = fsign(ydist);
        int steps = fabs(ydist);

        float px = x1;
        float py = y1;

        while(steps >= 0) {
            if ( _INBOUNDS((int)px, (int)py) )
                buffer[ CANVAS_ACCESS( (int)px, (int)py, w, h ) ] = c;
            px += dx;
            py += dy;

            //if ( _OOB( (int)px, (int)py ) ) { return; }
            steps--;
        }
    }

    #undef _INBOUNDS
}

#define _SLICE_LINE( _LINE, _LERP_COMPONENT, _SET_COMPONENT,  _OPERATOR, _AXIS, _OUT_ACTION ) \
{ \
    if ( _LINE.point[0]._SET_COMPONENT _OPERATOR _AXIS && \
         _LINE.point[1]._SET_COMPONENT _OPERATOR _AXIS ) { _OUT_ACTION } \
    \
    if ( _LINE.point[0]._SET_COMPONENT _OPERATOR _AXIS || \
         _LINE.point[1]._SET_COMPONENT _OPERATOR _AXIS ) { \
        \
        int outside_id, inside_id; \
        \
        if ( _LINE.point[0]._SET_COMPONENT _OPERATOR _AXIS ) \
            outside_id = 0; \
        else \
            outside_id = 1; \
        \
        inside_id = 1 - outside_id; \
        \
        float ydist = fabs( _LINE.point[inside_id]._SET_COMPONENT - _LINE.point[outside_id]._SET_COMPONENT ); \
        float in_length = fabs( _LINE.point[outside_id]._SET_COMPONENT - _AXIS ); \
        float in_ptg = in_length / ydist; \
        float out_ptg = 1.0 - in_ptg; \
        \
        _LINE.point[outside_id]._SET_COMPONENT = _AXIS; \
        _LINE.point[outside_id]._LERP_COMPONENT = \
            ( _LINE.point[outside_id]._LERP_COMPONENT * out_ptg ) + ( _LINE.point[inside_id]._LERP_COMPONENT * in_ptg ); \
    } \
}


//shape's side can be realloc'd.
//c --> case. 0 and 1 are horizontal slices, 1 keeps the +y area
//2 and 3 are vertical slices, 3 keeps the +x area

static void _BSEN_2D_shape_slice(BSEN_2D_shape* shape, float axis, int c) {

    uint8_t discarded[ shape->nsides ];
    memset( discarded, 0, shape->nsides );

    int aux_nsides = 0;
    BSEN_float2_line aux_sides[ shape->nsides + 1 ];

    for ( int k = 0; k < shape->nsides; k++ ) {

        switch(c) {
            case 0:
            _SLICE_LINE( shape->side[k], x, y,  >, axis, discarded[k] = 1; continue; )
            break;

            case 1:
            _SLICE_LINE( shape->side[k], x, y,  <, axis, discarded[k] = 1; continue; )
            break;

            case 2:
            _SLICE_LINE( shape->side[k], y, x,  >, axis, discarded[k] = 1; continue; )
            break;

            case 3:
            _SLICE_LINE( shape->side[k], y, x,  <, axis, discarded[k] = 1; continue; )
            break;

            default: break;
        }
    }

    for (int k = 0; k < shape->nsides; k++) {
        if ( discarded[k] ) { continue; }
        int next_side = -1;
        for (int j = 1; j < shape->nsides; j++) {
            int ns = (k + j) % shape->nsides;
            if ( !discarded[ns] ) { next_side = ns; break; }
        }

        if (next_side == -1) { return; }

        aux_sides[aux_nsides] = shape->side[k];
        aux_nsides++;

        //check for discontinuity
        if (  memcmp(&shape->side[k].point[1], &shape->side[next_side].point[0], sizeof(BSEN_float2)) != 0 ) {
            aux_sides[aux_nsides] = (BSEN_float2_line) { .point = {shape->side[k].point[1], shape->side[next_side].point[0] } };
            aux_nsides++;
        }
    }

    if (aux_nsides != shape->nsides && aux_nsides > 0) {
        shape->side = realloc( shape->side, aux_nsides * sizeof(BSEN_float2_line) );
        shape->nsides = aux_nsides;
    }

    if (aux_nsides > 0)
    memcpy(shape->side, aux_sides, aux_nsides * sizeof(BSEN_float2_line) );
    else {
        shape->nsides = 0;
    }
}

static inline BSEN_float2 _BSEN_calculate_plane_coords( const BSEN_context* ctx, int w, int real_h,
int px, int real_py, float z, int yshear, float fov_yplane_factor, float* ypixel_tan_factor,
float dynamic_xoffset, float dynamic_yoffset, float dynamic_angle,
uint8_t dynamic_plane, uint8_t translate ) {

    BSEN_float2 plane;

    int pyshear = (iclamp( (real_py - yshear), -real_h, 2 * real_h - 1 ) );

    plane.x = ypixel_tan_factor[ pyshear + real_h ] * fabs(z);
    plane.y = fov_yplane_factor * plane.x * (float)((px) - w/2) / (float)(w/2) ;

    if (dynamic_plane) {

        float cx = ctx->camera.pos.x + dynamic_xoffset;
        float cy = ctx->camera.pos.y + dynamic_yoffset;

        float cos_da = cos(dynamic_angle);
        float sin_da = sin(dynamic_angle);

        _BSEN_float2_rotate( &plane.x, &plane.y, -ctx->camera.hangle + dynamic_angle);

        if (translate) {
            plane.x += (cx)* cos_da - cy * sin_da ;
            plane.y += (cx)* sin_da + cy * cos_da ;
        }
    }
    else {

        _BSEN_float2_rotate( &plane.x, &plane.y, -ctx->camera.hangle);

        if (translate) {
            plane.x += ctx->camera.pos.x;
            plane.y += ctx->camera.pos.y;
        }
    }

    return plane;
}

static void _BSEN_render_flat(const BSEN_context* ctx, const BSEN_rendering_slice* render_slice,
const BSEN_flat* flat, int yshear) {

    if ( flat->type == BSEN_FLAT_TYPE_FLOOR && flat->z <= ctx->camera.pos.z ) { return; }
    if ( flat->type == BSEN_FLAT_TYPE_CEIL  && flat->z >= ctx->camera.pos.z ) { return; }

    float z = flat->z - ctx->camera.pos.z;
    int w = ctx->w;
    int h = render_slice->h;
    int real_h = ctx->h;

    float w_half = w / 2;
    float h_half = real_h / 2;

    BSEN_2D_shape shape;
    shape.nsides = flat->shape.nsides;
    shape.side = malloc( sizeof(BSEN_float2_line) * shape.nsides );
    memcpy( shape.side, flat->shape.side, shape.nsides * sizeof(BSEN_float2_line) );

    for (int k = 0; k < shape.nsides; k++) {
        shape.side[k].point[0].x -= ctx->camera.pos.x;
        shape.side[k].point[0].y -= ctx->camera.pos.y;
        shape.side[k].point[1].x -= ctx->camera.pos.x;
        shape.side[k].point[1].y -= ctx->camera.pos.y;
    }

    //1. slice flat with znear
    for (int k = 0; k < shape.nsides; k++) {
        _BSEN_float2_rotate( &shape.side[k].point[0].x, &shape.side[k].point[0].y, ctx->camera.hangle );
        _BSEN_float2_rotate( &shape.side[k].point[1].x, &shape.side[k].point[1].y, ctx->camera.hangle );
    }

    _BSEN_2D_shape_slice( &shape, BSEN_Z_NEAR, 3);
    if (shape.nsides <= 0) { goto free_and_return; }

    //turn map shape into screen shape
    for (int k = 0; k < shape.nsides; k++) {

        //assemble actual 3D coords of this side
        BSEN_float3_line line3d;
        for (int a = 0; a < 2; a++) {
            line3d.point[a].x = shape.side[k].point[a].x;
            line3d.point[a].y = shape.side[k].point[a].y;
            line3d.point[a].z = z;
        }

        BSEN_float2_line sc_line;
        for (int a = 0; a < 2; a++) {
             sc_line.point[a] = _BSEN_project_point( ctx, &line3d.point[a], yshear, w_half, h_half );
             sc_line.point[a].y -= render_slice->yoffset;
             shape.side[k].point[a] = (BSEN_float2) { (float)sc_line.point[a].x, (float)sc_line.point[a].y };
        }

    }

    #define _ROUND_SHAPE( _SHAPE ) \
    { \
        for (int k = 0; k < _SHAPE.nsides; k++) \
        for (int a = 0; a < 2; a++) { \
            _SHAPE.side[k].point[a].x = roundf( _SHAPE.side[k].point[a].x ); \
            _SHAPE.side[k].point[a].y = roundf( _SHAPE.side[k].point[a].y ); \
        } \
    }

    //slice to screen
    _BSEN_2D_shape_slice( &shape, 0, 1);     _ROUND_SHAPE( shape );
    _BSEN_2D_shape_slice( &shape, h - 1, 0); _ROUND_SHAPE( shape );
    _BSEN_2D_shape_slice( &shape, 0, 3);     _ROUND_SHAPE( shape );
    _BSEN_2D_shape_slice( &shape, w - 1, 2); _ROUND_SHAPE( shape );

    #undef _ROUND_SHAPE

    if (shape.nsides <= 0) { goto free_and_return; }

     _BSEN_slice_clear_flood(render_slice, w);

    for (int k = 0; k < shape.nsides; k++) {

        _BSEN_render_line( w, h, render_slice->flood_buffer, BSEN_FLOOD_BORDER,
        (int)shape.side[k].point[0].x,
        (int)shape.side[k].point[0].y,
        (int)shape.side[k].point[1].x,
        (int)shape.side[k].point[1].y );

        /*
        _BSEN_render_line( w, h, render_slice->pixel_buffer, 15,
        (int)shape.side[k].point[0].x,
        (int)shape.side[k].point[0].y,
        (int)shape.side[k].point[1].x,
        (int)shape.side[k].point[1].y );
        */

    }

    float fill_x = 0, fill_y = 0;

    for (int k = 0; k < shape.nsides; k++) {
        fill_x += shape.side[k].point[0].x;
        fill_y += shape.side[k].point[0].y;
    }

    fill_x /= shape.nsides;
    fill_y /= shape.nsides;

    {
        uint8_t* flood_buffer = render_slice->flood_buffer;
        uint8_t* pixel_buffer = render_slice->pixel_buffer;
        float*   depth_buffer = render_slice->depth_buffer;

        float*   ypixel_tan_factor   = ctx->plane_tables.ypixel_tan_factor;
        uint8_t* ypixel_light_factor = ctx->plane_tables.ypixel_light_factor;

        float fov_yplane_factor = _BSEN_FOV_DIVISION_FACTOR[ctx->camera.FOV];

        uint8_t dynamic_plane = flat->dynamic_plane;
        float dynamic_angle   = flat->dynamic_angle;
        float dynamic_xoffset = flat->dynamic_xoffset;
        float dynamic_yoffset = flat->dynamic_yoffset;

        int x = fill_x;
        int y = fill_y;

        //if (x <= 2 || x >= w - 2 || y <= 2 || y >= h - 2) { goto free_and_return; }

        int check_new_fill = 0;
        for (int dy = -1; dy <= 1; dy++)
        for (int dx = -1; dx <= 1; dx++) {
            int xx = x + dx;
            int yy = y + dy;
            if (xx < 0 || xx >= w || yy < 0 || yy >= h) { continue; }
            if ( flood_buffer[ CANVAS_ACCESS(xx, yy, w, h) ] == BSEN_FLOOD_BORDER ) {
                check_new_fill = 1;
                goto exit_for_dn;
            }
        }

        exit_for_dn:
        if (check_new_fill) {
            int xstart = x - 32;
            int xend   = x + 32;
            int ystart = y - 12;
            int yend   = y + 12;

            int potential_new_x, potential_new_y;
            int success = 0;

            for (int px = xstart; px <= xend; px++) {
                int stage = 0; //0 looking for first border, 1 found 1st border, looking for empty, 2 looking for last border, 3 success
                if (px < 0 || px >= w) { continue; }

                for (int py = ystart; py <= yend; py++) {
                    if (py < 0 || py >= h) { continue; }

                    switch(stage) {
                        case 0:
                        case 2:
                            if ( flood_buffer[ CANVAS_ACCESS(px, py, w, h) ] == BSEN_FLOOD_BORDER )
                                stage++;
                        break;

                        case 1:
                            if ( flood_buffer[ CANVAS_ACCESS(px, py, w, h) ] == BSEN_FLOOD_EMPTY ) {
                                stage++;
                                potential_new_x = px;
                                potential_new_y = py;
                            }
                        break;
                    }
                }

                if (stage > 2) {
                    x = potential_new_x;
                    y = potential_new_y;
                    success = 1;
                    break;
                }
            }

            if (!success) { goto free_and_return; }
        }

        float diminished_range = ctx->light.diminished_range;

        BSEN_int2 stored_location[2]; //stores up and down locations to remember to go back and fill
        //0 is up, 1 is down

        uint8_t valid_stored_location[2] = {0, 0}; //bools

        uint8_t must_repeat;

        do {

            must_repeat = 0;

            if (valid_stored_location[0]) {
                x = stored_location[0].x;
                y = stored_location[0].y;

                valid_stored_location[0] = 0;
                must_repeat = 1;
            }
            else if (valid_stored_location[1]) {
                x = stored_location[1].x;
                y = stored_location[1].y;

                valid_stored_location[1] = 0;
                must_repeat = 1;
            }

            if (x < 0 || x >= w || y < 0 || y >= h) { continue; }

            int real_py = y + render_slice->yoffset;

            /*
            int real_py = y + render_slice->yoffset;
            //int pyshear = (iclamp( (y - yshear), -h, 2*h - 1 ) );
            int pyshear = (iclamp( (real_py - yshear), -real_h, 2 * real_h - 1 ) );

            float plane_x[2];
            float plane_y[2];
            plane_x[0] = ypixel_tan_factor[ pyshear + real_h ] * fabs(z);
            plane_y[0] = fov_yplane_factor * plane_x[0] * (float)((x) - w/2) / (float)(w/2) ;

            //float depth = _BSEN_float3_magnitude(plane_x[0], plane_y[0], z);

            plane_y[1] = fov_yplane_factor * plane_x[0] * (float)((x+1) - w/2) / (float)(w/2) ;
            //float depth_delta = fabs( _BSEN_float3_magnitude(plane_x[0], plane_y[1], z)  - depth);

            plane_x[1] = plane_x[0];

            float pre_translation_plane_x;
            float pre_translation_plane_y;

            if (dynamic_plane) {

                float cx = ctx->camera.pos.x + dynamic_xoffset;
                float cy = ctx->camera.pos.y + dynamic_yoffset;

                float cos_da = cos(dynamic_angle);
                float sin_da = sin(dynamic_angle);

                for (int p = 0; p < 2; p++) {

                    _BSEN_float2_rotate( &plane_x[p], &plane_y[p], -ctx->camera.hangle + dynamic_angle);

                    if (p == 0) {
                        pre_translation_plane_x = plane_x[0];
                        pre_translation_plane_y = plane_y[0];
                    }

                    plane_x[p] += (cx)* cos_da - cy * sin_da ;
                    plane_y[p] += (cx)* sin_da + cy * cos_da ;

                }

            }
            else {

                for (int p = 0; p < 2; p++) {

                    _BSEN_float2_rotate( &plane_x[p], &plane_y[p], -ctx->camera.hangle);

                    if (p == 0) {
                        pre_translation_plane_x = plane_x[0];
                        pre_translation_plane_y = plane_y[0];
                    }

                    plane_x[p] += ctx->camera.pos.x;
                    plane_y[p] += ctx->camera.pos.y;

                }
            }
            */


            BSEN_float2 plane_current = _BSEN_calculate_plane_coords( ctx, w, real_h, x, real_py, z, yshear,
                fov_yplane_factor, ypixel_tan_factor, dynamic_xoffset, dynamic_yoffset, dynamic_angle,
                dynamic_plane, 1 );

            BSEN_float2 plane_left = _BSEN_calculate_plane_coords( ctx, w, real_h, x + 1, real_py, z, yshear,
                fov_yplane_factor, ypixel_tan_factor, dynamic_xoffset, dynamic_yoffset, dynamic_angle,
                dynamic_plane, 1 );

            BSEN_float2 pre_translation_plane = _BSEN_calculate_plane_coords( ctx, w, real_h, x, real_py, z, yshear,
                fov_yplane_factor, ypixel_tan_factor, dynamic_xoffset, dynamic_yoffset, dynamic_angle,
                0, 0 );

            float plane_dx = plane_left.x -  plane_current.x;
            float plane_dy = plane_left.y -  plane_current.y;

            //horizontal fill
            int original_x = x;

            float scan_plx;
            float scan_ply;
            float scan_pre_plx;
            float scan_pre_ply;

            float depth;
            float prev_depth = NAN;

            float z_bias = ( z > 0 ) ? 1 : -1;

            int depth_scan_delay = 0;

            float plane_dx_up = NAN;
            float plane_dy_up = NAN;

            float plane_dx_down = NAN;
            float plane_dy_down = NAN;

            BSEN_float2 plane_up   = (BSEN_float2){NAN, NAN};
            BSEN_float2 plane_down = (BSEN_float2){NAN, NAN};

            for (int dx = -1; dx <= 1; dx+= 2) {
                x = original_x;
                scan_plx = plane_current.x;
                scan_ply = plane_current.y;
                scan_pre_plx = pre_translation_plane.x;
                scan_pre_ply = pre_translation_plane.y;

                int depth_scan_delay = 0;

                while (1) {

                    if ( depth_scan_delay <= 0 ) {
                        depth = _BSEN_float3_magnitude(scan_pre_plx, scan_pre_ply, z + z_bias);
                        depth_scan_delay = ctx->depth_precision;
                    }
                    else { depth_scan_delay--; }

                    int id = CANVAS_ACCESS(x, y, w, h);

                    if ( x >= w || x < 0 ) {
                        break;
                    }

                    if ( depth_buffer[id] > depth ) {

                        int tx = abs((int)scan_plx) % flat->tx->w;
                        int ty = abs((int)scan_ply) % flat->tx->h;

                        BSEN_bytecolor c = flat->tx->data[tx + ty * flat->tx->w];
                        float light;
                        uint8_t intensity;

                        if (c != BSEN_COLOR_ALPHA_KEY) {

                            light = fmax( diminished_range - depth, 0.0 ) / diminished_range;
                            intensity = c & BSEN_INTENSITY_MASK;
                            intensity *= light;
                            c = (c & (~BSEN_INTENSITY_MASK)) | (intensity & BSEN_INTENSITY_MASK) ;

                            pixel_buffer[id] = c;
                            depth_buffer[id] = depth;
                        }

                        //in order to color borders touching the top/bottom of the screen, since this
                        //algorithm can't really color them otherwise
                        if ( y == 1 || y == h - 2) {

                            float* plane_vstep_dx;
                            float* plane_vstep_dy;
                            BSEN_float2* plane_vstep;

                            int _border_dy = 1;
                            if (y == 1) {
                                _border_dy = -1;
                                plane_vstep_dx = &plane_dx_up;
                                plane_vstep_dy = &plane_dy_up;
                                plane_vstep = &plane_up;
                            }
                            else {
                                plane_vstep_dx = &plane_dx_down;
                                plane_vstep_dy = &plane_dy_down;
                                plane_vstep = &plane_down;
                            }

                            if ( isnan( *plane_vstep_dx ) ) {
                                *plane_vstep = _BSEN_calculate_plane_coords( ctx, w, real_h, x, real_py + _border_dy,
                                    z, yshear, fov_yplane_factor, ypixel_tan_factor, dynamic_xoffset, dynamic_yoffset, dynamic_angle,
                                    dynamic_plane, 1 );

                                BSEN_float2 plane_vstep_left = _BSEN_calculate_plane_coords( ctx, w, real_h, x + 1, real_py + _border_dy,
                                    z, yshear, fov_yplane_factor, ypixel_tan_factor, dynamic_xoffset, dynamic_yoffset, dynamic_angle,
                                    dynamic_plane, 1 );

                                *plane_vstep_dx = plane_vstep_left.x - plane_vstep->x;
                                *plane_vstep_dy = plane_vstep_left.y - plane_vstep->y;
                            }

                            int x_from_scanline_origin = x - original_x;

                            float vstep_scan_plx = plane_vstep->x + x_from_scanline_origin * (*plane_vstep_dx);
                            float vstep_scan_ply = plane_vstep->y + x_from_scanline_origin * (*plane_vstep_dy);

                            int vstep_tx = abs( (int) (vstep_scan_plx) ) % flat->tx->w;
                            int vstep_ty = abs( (int) (vstep_scan_ply) ) % flat->tx->h;

                            int _border_id = CANVAS_ACCESS(x, y + _border_dy, w, h);

                            if (flood_buffer[_border_id] == BSEN_FLOOD_BORDER ) {

                                c = flat->tx->data[vstep_tx + vstep_ty * flat->tx->w];

                                if (c != BSEN_COLOR_ALPHA_KEY ) {

                                    intensity = c & BSEN_INTENSITY_MASK;
                                    intensity *= light;
                                    c = (c & (~BSEN_INTENSITY_MASK)) | (intensity & BSEN_INTENSITY_MASK) ;

                                    pixel_buffer[_border_id] = c;
                                    depth_buffer[_border_id] = depth;
                                }
                            }
                        }
                    }

                    if ( flood_buffer[id] == BSEN_FLOOD_BORDER ) { break; }

                    flood_buffer[id] = BSEN_FLOOD_FILLED;

                    for (int k = 0; k < 2; k++) {

                        int dy = -1;
                        if (k > 0) { dy = 1; }

                        int yy = y + dy;
                        if (yy < 0 || yy >= h) { continue; }

                        if ( valid_stored_location[k] == 0 )
                        if ( flood_buffer[ CANVAS_ACCESS(x, yy, w, h) ] == BSEN_FLOOD_EMPTY )
                        {
                            stored_location[k].x = x;
                            stored_location[k].y = yy;
                            valid_stored_location[k] = 1;
                            must_repeat = 1;
                        }
                    }



                    x+=dx;
                    scan_plx += dx * plane_dx;
                    scan_ply += dx * plane_dy;
                    scan_pre_plx += dx * plane_dx;
                    scan_pre_ply += dx * plane_dy;

                    prev_depth = depth;
                }
            }
        } while (must_repeat);

    }

    free_and_return:

    /*
    //temp red point
    {
        int rx = fill_x;
        int ry = fill_y;

        if (rx >= 0 && rx < w && ry >= 0 && ry < h) {
            ctx->pixel_buffer[ CANVAS_ACCESS(rx, ry, w, h) ] = 255;
        }
    }
    */

    free(shape.side);
    return;
}


static void _BSEN_render_model( BSEN_context* ctx, BSEN_model_instance* instance, int yshear ) {

    int w = ctx->w;
    int h = ctx->h;

    float w_half = w / 2;
    float h_half = h / 2;

    BSEN_float3 pos = instance->pos;
    pos.x -= ctx->camera.pos.x;
    pos.y -= ctx->camera.pos.y;
    pos.z -= ctx->camera.pos.z;

    float diminished_range = ctx->light.diminished_range;

    float ztop       = pos.z + instance->model->zmin;
    float zbottom    = pos.z + instance->model->zmax;

    for (int py = 0; py < instance->model->ysize; py++)
    for (int px = 0; px < instance->model->xsize; px++) {
        BSEN_voxel_array* va = &instance->model->va_grid[ px + instance->model->xsize * py ];

        if ( va->nvoxels <= 0 ) { continue; }

        BSEN_float2 xy_pos = (BSEN_float2) {
            .x = px - instance->model->xoffset,
            .y = py - instance->model->yoffset
        };

        _BSEN_float2_rotate( &xy_pos.x, &xy_pos.y, instance->hangle );

        xy_pos.x += pos.x;
        xy_pos.y += pos.y;

        _BSEN_float2_rotate( &xy_pos.x, &xy_pos.y, ctx->camera.hangle );

        if ( xy_pos.x < BSEN_Z_NEAR ) { continue; }

        float depth = _BSEN_float2_magnitude( xy_pos.x, xy_pos.y );

        BSEN_float2 sp_bottomright = _BSEN_project_point( ctx,
            &(BSEN_float3){.x = xy_pos.x, .y = xy_pos.y + 0.5, .z = zbottom},
            yshear, w_half, h_half
        );

        BSEN_float2 sp_topleft = _BSEN_project_point( ctx,
            &(BSEN_float3){.x = xy_pos.x, .y = xy_pos.y - 0.5, .z = ztop},
            yshear, w_half, h_half
        );

        float xcenter = (sp_bottomright.x + sp_topleft.x) / 2.0;

        int xsquare_size = ceilf( (1.0 + 2.0*fabs(xcenter - w_half) / w_half) * (sp_bottomright.x - sp_topleft.x) / 2.0 );
        if ( xsquare_size > w / 4 ) { xsquare_size = w / 4; }

        float zsize = (float) (instance->model->zmax - instance->model->zmin);

        for (int v = 0; v < va->nvoxels; v++) {

            int view_z = va->varray[v].z;
            int view_zmin = instance->model->zmin;
            int view_zmax = instance->model->zmax;

            float ptg = (float)( va->varray[v].z - instance->model->zmax ) / zsize;

            float fy = ( ( sp_bottomright.y * ptg ) + ( sp_topleft.y * (1.0 - ptg) ) );
            int y = roundf(fy);
            int x = roundf( xcenter );

            int ysquare_size = roundf( (1.0 + 2.0*fabs(fy - h_half - yshear) / h_half) * (sp_bottomright.x - sp_topleft.x) / 2.0 );
            if ( ysquare_size > h / 4 ) { continue; }

            for (int sy = y - ysquare_size; sy <= y + ysquare_size; sy++)
            for (int sx = x - xsquare_size; sx <= x + xsquare_size; sx++) {

                if ( sy < 0 || sy >= h ) { continue; }
                if ( sx < 0 || sx >= w ) { continue; }

                int sid = CANVAS_ACCESS(sx, sy, w, h);

                if ( ctx->depth_buffer[ sid ] > depth ) {

                    float light = fmax( diminished_range - depth, 0.0 ) / diminished_range;

                    BSEN_bytecolor c = va->varray[v].c;

                    uint8_t intensity = c & BSEN_INTENSITY_MASK;
                    intensity *= light;
                    c = (c & (~BSEN_INTENSITY_MASK)) | (intensity & BSEN_INTENSITY_MASK) ;

                    ctx->pixel_buffer[ sid ] = c;
                    ctx->depth_buffer[ sid ] = depth;

                }
            }
        }
    }
}


static void _BSEN_render_sprite( const BSEN_context* ctx, const BSEN_rendering_slice* render_slice,
BSEN_3D_sprite spr, int yshear ) {

    spr.pos.z += spr.zoffset;

    float w2 = ((float)spr.sprite->h) / 2.0;

    BSEN_float2 left  = (BSEN_float2){ 0, -w2 };
    BSEN_float2 right = (BSEN_float2){ 0,  w2 };

    float theta = -ctx->camera.hangle;

    _BSEN_float2_rotate( &left.x,  &left.y,  theta );
    _BSEN_float2_rotate( &right.x, &right.y, theta );

    BSEN_wall twall;
    twall.tx = spr.sprite;
    twall.line.point[0] = (BSEN_float2){ spr.pos.x + left.x,  spr.pos.y + left.y  };
    twall.line.point[1] = (BSEN_float2){ spr.pos.x + right.x, spr.pos.y + right.y };

    twall.point_texture_y[0] = 0;
    twall.point_texture_y[1] = spr.sprite->h;

    twall.base_z = spr.pos.z;
    twall.draw_from_behind = 1;
    twall.height = spr.sprite->w;

    twall.base_texture_x = 0;
    twall.top_texture_x  = spr.sprite->w - 1;

    _BSEN_render_wall( ctx, render_slice, &twall, yshear );

}

static void _BSEN_context_render_slice(BSEN_context* ctx, BSEN_rendering_slice* slice) {

    _BSEN_slice_clear_color(slice, ctx->w);
    _BSEN_slice_clear_depth(slice, ctx->w);

    ctx->camera.yshear = fmin(fmax(ctx->camera.yshear, -1.0), 1.0);
    int yshear = ctx->camera.yshear * ctx->h;

    for (int k = 0; k < ctx->temp_scene.nflats; k++) {
        _BSEN_render_flat(ctx, slice, ctx->temp_scene.flats[k], yshear);
    }

    for (int k = 0; k < ctx->temp_scene.nwalls; k++) {
        _BSEN_render_wall(ctx, slice, ctx->temp_scene.walls[k], yshear);
    }

    for (int k = 0; k < ctx->temp_scene.nsprites; k++) {
        _BSEN_render_sprite(ctx, slice, ctx->temp_scene.sprites[k], yshear);
    }

}

static int _BSEN_thread_main(void* data) {

    SDL_SetThreadPriority( SDL_THREAD_PRIORITY_HIGH );

    BSEN_thread_data* thread_data = (BSEN_thread_data*)data;
    int quit = 0;

    while( !quit ) {

        //sleep thread until signaled to start
        SDL_mutexP( thread_data->ctx->thread_awaker_access );
        SDL_CondWait( thread_data->ctx->thread_awaker, thread_data->ctx->thread_awaker_access );
        SDL_mutexV( thread_data->ctx->thread_awaker_access );

        switch( thread_data->directive ) {

            case BSEN_THREAD_DIRECTIVE_QUIT: quit = 1; break;

            //////////////////
            case BSEN_THREAD_DIRECTIVE_RENDER:
            {
                _BSEN_context_render_slice( thread_data->ctx, thread_data->render_slice );
                SDL_SemPost( thread_data->thread_finished ); //signal main thread that rendering finished here
            }
            break;
        }

    }

    return 0;
}

BSEN_context* BSEN_context_create(int w, int h, int nthreads) {

    BSEN_context* ctx = malloc( sizeof(BSEN_context) );
    ctx->w = w;
    ctx->h = h;
    ctx->pixel_buffer = malloc( w * h * sizeof(BSEN_bytecolor) );
    ctx->depth_buffer = malloc( w * h * sizeof(float) );
    ctx->flood_buffer = malloc( w * h * sizeof(uint8_t) );

    BSEN_context_camera_pos  ( ctx, 0, 0, 0 );
    BSEN_context_camera_angle( ctx, 0, 0);
    BSEN_context_camera_FOV  ( ctx, BSEN_FOV_127 );
    BSEN_context_clear_color ( ctx );
    BSEN_context_clear_depth ( ctx );

    ctx->temp_scene.nwalls = 0;
    ctx->temp_scene.walls  = malloc( 4 * 64*64 * sizeof(BSEN_wall*) );

    ctx->temp_scene.nflats = 0;
    ctx->temp_scene.flats  = malloc( 64*64 * sizeof(BSEN_flat*) );

    ctx->temp_scene.nmodels = 0;
    ctx->temp_scene.models  = malloc( 64*64 * sizeof(BSEN_model_instance*) );

    ctx->temp_scene.nsprites = 0;
    ctx->temp_scene.sprites = malloc( 64*64 * sizeof(BSEN_3D_sprite) );

    ctx->plane_tables.ypixel_tan_factor = malloc(3 * h * sizeof(float));

    //3*h since yshear can go from -1 to 1
    for (int py = 0; py < 3*h; py++) {
        float tan_half_fov = tan( _BSEN_HALF_FOV[BSEN_VFOV] );
        int yy = py - h;
        float yptg =  ( (float)abs(yy - (h/2) ) ) / (float) (h / 2);
        const float min_yptg = 0.01;
        if (yptg < min_yptg) { yptg = min_yptg; }
        ctx->plane_tables.ypixel_tan_factor[py] = ( 1 / ( yptg ) ) / ( tan_half_fov );
    }

    ctx->nthreads = nthreads;
    ctx->render_slice    = malloc ( (1 + nthreads) * sizeof( BSEN_rendering_slice ) );
	ctx->thread_data     = malloc ( nthreads * sizeof( BSEN_thread_data ) );
	ctx->thread          = malloc ( nthreads * sizeof( SDL_Thread* ) );

	//initialize render slices
    int nSlices = 1 + nthreads;
    int h_slc = ctx->h / nSlices;
    for ( int k = 0; k < nSlices; k++ ) {
        ctx->render_slice[k].h = h_slc;
        ctx->render_slice[k].yoffset = k * h_slc;

        int access = 0 + ctx->w * ctx->render_slice[k].yoffset;

        ctx->render_slice[k].depth_buffer = &ctx->depth_buffer[access];
        ctx->render_slice[k].flood_buffer = &ctx->flood_buffer[access];
        ctx->render_slice[k].pixel_buffer = &ctx->pixel_buffer[access];

        if (k == nSlices - 1) { //adjust height of last slice just in case it isn't quite the right size
            int hbottom = ctx->render_slice[k].yoffset + ctx->render_slice[k].h;
            int dh = ctx->h - hbottom;
            ctx->render_slice[k].h += dh;
        }
    }

    //initialize thread data
    for ( int k = 0; k < nthreads; k++ ) {
        ctx->thread_data[k].ctx = ctx;
        ctx->thread_data[k].directive = BSEN_THREAD_DIRECTIVE_RENDER;
        ctx->thread_data[k].render_slice = & ctx->render_slice[k + 1];
        ctx->thread_data[k].thread_finished = SDL_CreateSemaphore(1); //1 aka binary semaphore
    }

	ctx->thread_awaker_access = SDL_CreateMutex();
    ctx->thread_awaker = SDL_CreateCond();

    for ( int k = 0; k < nthreads; k++) {

        char name[16];
        sprintf(name, "%THREAD(d)", k );
        ctx->thread[k] = SDL_CreateThread( _BSEN_thread_main, name, &ctx->thread_data[k] );
    }

    SDL_Delay(100); //Give the threads some time to reach the condition wait instruction

    ctx->light.diminished_range = 1000.0;
    ctx->light.directional_angle = 0;
    ctx->light.directional_min = 0.5;

    ctx->depth_precision = 6; //aka delay between per pixel depth calculations

    return ctx;
}


void BSEN_context_render(BSEN_context* ctx) {

    BSEN_context_clear_color(ctx);
    BSEN_context_clear_depth(ctx);

    for (int k = 0; k < ctx->nthreads; k++) {
        SDL_SemTryWait( ctx->thread_data[k].thread_finished ); //mark thread as not finished rendering yet
    }

    SDL_CondBroadcast( ctx->thread_awaker );

    _BSEN_context_render_slice( ctx, &ctx->render_slice[0] );

    for (int k = 0; k < ctx->nthreads; k++) {
        SDL_SemWait( ctx->thread_data[k].thread_finished );
        SDL_SemPost( ctx->thread_data[k].thread_finished );
    }
}
