#ifndef _BSEN_APP_H
#define _BSEN_APP_H

#include "BSEN.h"
#include <stdint.h>

typedef struct {
	BSEN_context* ctx;
	char quit;
	char clear_window_queued;

	struct {
		char mouse_held[2]; //0 left, 1 right
		int mouse_xrel, mouse_yrel;
	} input;

	void* _private;
} BSEN_app;

BSEN_app* BSEN_app_create(const char* display_name, int w, int h, int nthreads);
void 	  BSEN_app_refresh_inputs(BSEN_app* app);
void	  BSEN_app_refresh_window(BSEN_app* app); //doesn't render anything, just copies pixel data into the screen
void 	  BSEN_app_set_window_size(BSEN_app* app, int w, int h);
int 	  BSEN_app_check_keyboard(BSEN_app* app, int key);

enum _BSEN_SET_MOUSE_MODES {
	BSEN_MOUSE_MODE_NORMAL = 0,
	BSEN_MOUSE_MODE_RELATIVE,
	BSEN_MOUSE_MODE_SWAP
};

void BSEN_set_mouse_mode(int mode);

int64_t BSEN_get_ms(void);
void BSEN_delay(int ms);

void BSEN_set_high_priority();

#endif
