#include "BSEN_app.h"
#include "SDL.h"
#include "SDL_opengl.h"
#include <stdlib.h>
#include <stdio.h>

#ifdef _WIN32
#include <windows.h>
#endif

#ifdef __linux__
#include <unistd.h>
#include <sys/resource.h>
#endif

typedef struct {
	SDL_Window* window;
	SDL_GLContext context;
	const uint8_t* keyboard;
	BSEN_rgb* tempBuffer;
	BSEN_rgb  tempColormap[256];
} _BSEN_app_private;

BSEN_app* BSEN_app_create(const char* display_name, int w, int h, int nthreads) {

	 _BSEN_app_private* _private = malloc(sizeof(_BSEN_app_private));

	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | SDL_INIT_AUDIO) < 0) {
        printf("SDL_Init: %s\n", SDL_GetError());
        free(_private);
        return NULL;
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);

    _private->window = SDL_CreateWindow(display_name,
                               SDL_WINDOWPOS_UNDEFINED,
                               SDL_WINDOWPOS_UNDEFINED,
                               w,
                               h,
                               SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL);

    if (_private->window == NULL) {
        printf("SDL_CreateWindow: %s\n", SDL_GetError());
        free(_private);
        return NULL;
    }

	_private->context = SDL_GL_CreateContext(_private->window);

	if (_private->context == NULL) {
        printf("SDL_GL_CreateContext: %s\n", SDL_GetError());
        free(_private);
        return NULL;
	}

	_private->keyboard = SDL_GetKeyboardState(NULL);

	_private->tempBuffer = malloc(sizeof(BSEN_rgb) * w * h);

    glViewport(0, 0, w, h);
    SDL_GL_SetSwapInterval(0);

	BSEN_app* app = malloc(sizeof(BSEN_app));
	app->quit = 0;
	app->ctx = BSEN_context_create(w, h, nthreads);
	app->input.mouse_held[0] = 0;
	app->input.mouse_held[1] = 0;
	app->input.mouse_xrel = 0;
	app->input.mouse_yrel = 0;
	app->clear_window_queued = 0;
	app->_private = _private;

	return app;
}


void BSEN_app_refresh_inputs(BSEN_app* app) {

	char hadMouseMotion = 0;

	SDL_Event e;
	while (SDL_PollEvent(&e)) {
			switch(e.type) {

				case SDL_QUIT:
					app->quit = 1;
				break;

				case SDL_WINDOWEVENT:
					if (e.window.event == SDL_WINDOWEVENT_RESIZED || e.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {

						SDL_Window* window = ((_BSEN_app_private*)app->_private)->window;
						int w,h;
						SDL_GetWindowSize(window, &w, &h);
						glViewport(0, 0, w, h);
						app->clear_window_queued = 1;
					}
				break;


				case SDL_MOUSEBUTTONDOWN:
				{
					char bt = 1;
					if ((uint8_t)(e.button.button) == SDL_BUTTON_LEFT) { bt = 0; }
					app->input.mouse_held[(int)bt] = 1;
				}
				break;

				case SDL_MOUSEBUTTONUP:
				{
					char bt = 1;
					if ((uint8_t)(e.button.button) == SDL_BUTTON_LEFT) { bt = 0; }
					app->input.mouse_held[(int)bt] = 0;
				}
				break;

				case  SDL_MOUSEMOTION:
				{
					hadMouseMotion = 1;
					app->input.mouse_xrel = e.motion.xrel;
					app->input.mouse_yrel = e.motion.yrel;
				}
				break;
			}
	}

	if (hadMouseMotion == 0) {
		app->input.mouse_xrel = 0;
		app->input.mouse_yrel = 0;
	}

	((_BSEN_app_private*)app->_private)->keyboard = SDL_GetKeyboardState(NULL);
}

int BSEN_app_check_keyboard(BSEN_app* app, int key) {
	return (int)((_BSEN_app_private*)app->_private)->keyboard[key];
}

void BSEN_app_refresh_window(BSEN_app* app) {

	if (app->clear_window_queued != 0) {
		glClearColor (0.0, 0.0, 0.0, 1.0);
		glClear (GL_COLOR_BUFFER_BIT);
		app->clear_window_queued = 0;
	}

	_BSEN_app_private* _private = app->_private;

	for (int yy = 0; yy < app->ctx->h; yy++)
	for (int xx = 0; xx < app->ctx->w; xx++) {
		#define PIXEL_ID (xx + app->ctx->w*yy)
		_private->tempBuffer[PIXEL_ID] = app->ctx->color_palette[ app->ctx->pixel_buffer[PIXEL_ID] ];
		#undef PIXEL_ID
	}

	SDL_Window* window = ((_BSEN_app_private*)app->_private)->window;

	int win_x, win_y;
	SDL_GetWindowSize(window, &win_x, &win_y);

	int w,h;
	w = app->ctx->w;
	h = app->ctx->h;

	//for now, we only support stretch scalling mode
	float xscale = (float)win_x / (float)w;
	float yscale = (float)win_y / (float)h;

	glRasterPos2i(-1, 1);
	glPixelZoom(xscale, -yscale);
	glDrawPixels(w, h, GL_RGB, GL_UNSIGNED_BYTE, (const void*) _private->tempBuffer );

	SDL_GL_SwapWindow(window);
}

void BSEN_delay(int ms) {
	SDL_Delay(ms);
}

int64_t BSEN_get_ms(void) {
	return (int64_t)SDL_GetTicks();
}

void BSEN_set_mouse_mode(int mode) {
	switch(mode) {
		case BSEN_MOUSE_MODE_NORMAL:   SDL_SetRelativeMouseMode(SDL_FALSE); break;
		case BSEN_MOUSE_MODE_RELATIVE: SDL_SetRelativeMouseMode(SDL_TRUE); break;
		case BSEN_MOUSE_MODE_SWAP:     SDL_SetRelativeMouseMode(!SDL_GetRelativeMouseMode()); break;
	}
}

void BSEN_app_set_window_size(BSEN_app* app, int w, int h) {
	SDL_SetWindowSize(((_BSEN_app_private*)app->_private)->window, w, h);
}


void BSEN_set_high_priority() {
	#ifdef _WIN32
		SetPriorityClass(GetCurrentProcess(), HIGH_PRIORITY_CLASS);
	#endif

	#ifdef __linux__
		setpriority(PRIO_PROCESS, 0, -14);
	#endif
}
