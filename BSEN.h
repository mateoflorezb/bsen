#ifndef _BSEN_H
#define _BSEN_H

//called Z because that's how it's tipically called, but in the logic it's like X near
#define BSEN_Z_NEAR 0.05
//Z far would be the value that the every pixel in the depth buffer is set to when cleared
#define BSEN_Z_FAR 4000.0

#define BSEN_FLOOD_EMPTY  (0)
#define BSEN_FLOOD_BORDER (1)
#define BSEN_FLOOD_FILLED (2)

#define BSEN_MAX_SHARD_POINTS (12)

#define BSEN_FOV_90  (0)
#define BSEN_FOV_127 (1)
#define BSEN_FOV_152 (2)

#define BSEN_VFOV ( BSEN_FOV_127 )

#define BSEN_INTENSITY_LEVELS (16)
#define BSEN_COLOR_GROUPS (16)
#define BSEN_COLOR_GROUP_SHIFT (4)

#define BSEN_INTENSITY_MASK (0b00001111)

#define BSEN_FLAT_TYPE_FLOOR (1)
#define BSEN_FLAT_TYPE_CEIL  (2)
#define BSEN_FLAT_TYPE_BOTH  (3)

#define BSEN_COLOR_ALPHA_KEY (255)

#include <stdint.h>
#include <SDL_thread.h>

typedef struct {
	float x,y;
} BSEN_float2;

typedef struct {
	float x,y,z;
} BSEN_float3;

typedef struct {
	int x,y;
} BSEN_int2;

typedef struct {
    uint8_t r, g, b;
} BSEN_rgb;

typedef struct {
    BSEN_float2 point[2];
} BSEN_float2_line;

typedef struct {
    BSEN_float3 point[2];
} BSEN_float3_line;

typedef struct {
    BSEN_int2 point[2];
} BSEN_int2_line;

//bytecolor values are encoded as:
//GGGG IIII
//where G means group and I means intensity. We have 16 color groups and
//from each group, 16 levels of intensity
typedef uint8_t BSEN_bytecolor;

typedef struct {
	BSEN_bytecolor* data;
	int w,h;
} BSEN_texture;

//takes R-channel as bytecolor
BSEN_texture* BSEN_texture_load_png_rchannel(const char* filename);

//rendering code may not work properly if sent a concave 2D shape. Stick to convex
typedef struct {
    BSEN_float2_line* side;
    int nsides;
} BSEN_2D_shape;

//a floor or ceiling
//must be a convex shape
//points must be defined in consecutive order
typedef struct {
	float z; //aka height
	BSEN_texture* tx;
	uint8_t type; //see BSEN_FLAT_TYPE_<...>
    BSEN_2D_shape shape;
    uint8_t dynamic_plane; //(bool) set to true to allow dynamic translation and rotation
    float dynamic_angle; //these three are ignored if dynamic_plane is false
    float dynamic_xoffset;
    float dynamic_yoffset;

} BSEN_flat;

//so, for wall texture mapping:
//vertical columns of wall pixels are to be mapped to horizontal rows of texture.
//The texture should probably tile on both axis.
//Texcoords are in the actual range of the dimensions of the texture (compromise for speed)

//As for the depth test, find the depth of each of the corners and then lerp across both sides.
typedef struct {
	float base_z;
	float height;
    BSEN_float2_line line;
    float point_texture_y[2];
    float base_texture_x;
    float top_texture_x;
    uint8_t draw_from_behind; //bool
	BSEN_texture* tx;
} BSEN_wall;

typedef struct {
    int x,y,z;
    BSEN_bytecolor c;
} BSEN_voxel_in_file;

typedef struct {
    int z;
    BSEN_bytecolor c;
} BSEN_voxel_array_entry;

typedef struct {
    int nvoxels;
    BSEN_voxel_array_entry* varray;
} BSEN_voxel_array;

typedef struct {
    int16_t xsize;
    int16_t ysize;
    int16_t zmin;
    int16_t zmax;
    int16_t xoffset; //the smallest x coord + xoffset should be equal to 0
    int16_t yoffset; //same here

    BSEN_voxel_array* va_grid; //access with x + w*y (with the smallest x being 0)

} BSEN_model;

typedef struct {
    BSEN_model* model;
    BSEN_float3 pos;
    float hangle;
    float scale;
} BSEN_model_instance;

//zoffset is provided since most sprites are centered at the center, bottom, but
//some like imp fireballs may be best to center at center, middle
typedef struct {
    BSEN_float3 pos; //sprite is always centered at w/2, h - zoffset.
    BSEN_texture* sprite;
    float zoffset;
} BSEN_3D_sprite; //doom style enemies


//for multi threading, each thread renders a horizontal slice of the screen. This struct defines
//the parameters of such slice. Rendering functions need to draw stuff according to this
typedef struct {
    int h; //height in pixels of the slice
    int yoffset; //offset in pixels from the top of the screen

	BSEN_bytecolor* pixel_buffer;
	float* depth_buffer;
	uint8_t* flood_buffer;

} BSEN_rendering_slice;

enum _BSEN_THREAD_DIRECTIVES {
    BSEN_THREAD_DIRECTIVE_QUIT = 0,
    BSEN_THREAD_DIRECTIVE_RENDER
} ;

struct BSEN_context;
typedef struct BSEN_context BSEN_context;

typedef struct {

    BSEN_rendering_slice* render_slice;
    BSEN_context* ctx;
    int directive;
    SDL_sem* thread_finished;

} BSEN_thread_data;

////////////////////////
struct BSEN_context {
	int w,h; //virtual resolution

    /*
    2D array of pixels. Access with [thread][x + w*y].
    # of pixels is w * h
	*/
	BSEN_bytecolor* pixel_buffer;

	//same as above but for depth testing
	float* depth_buffer;

	//same as above but for the flood fill algorithm used for floors/ceils.
	//stores just false/true per pixel, and gets memset each time a new floor is to be drawn.
	//technically each pixel wastes 7 bits but literally who cares
	uint8_t* flood_buffer;

	struct {
		BSEN_float3 pos;
		float hangle;
		float yshear; //from -1 to 1 probably
		uint8_t FOV; //one of BSEN_FOV_<...> values.

	} camera;

	int depth_precision; //how much delay between depth calculations

	BSEN_rgb color_palette[256];

    //precalculated tables for plane stuff
    struct {
        float* ypixel_tan_factor; //access with y pixel. Assumes VFOV = 127
        uint8_t* ypixel_light_factor; //access with [ intensity +  py * BSEN_INTENSITY_LEVELS ]
    } plane_tables;

	struct {
        BSEN_wall** walls;
        int nwalls;

        int nflats;
        BSEN_flat** flats;

        int nmodels;
        BSEN_model_instance** models;

        int nsprites;
        BSEN_3D_sprite* sprites;

	} temp_scene;

	struct {
        float diminished_range;
        float directional_angle; //distance from this angle will be used to judge directional light for walls
        float directional_min; //at 180 degrees, this is the directional light factor
	} light;

	int nthreads;
	BSEN_thread_data* thread_data; //1D array, size = nthreads
	BSEN_rendering_slice* render_slice; //1D array, size = nthreads + 1 (+1 for the main thread, which is id 0)
	SDL_Thread** thread; //1D array, size = nthreads

	SDL_mutex* thread_awaker_access;
    SDL_cond*  thread_awaker;

};

BSEN_context* BSEN_context_create(int w, int h, int nthreads); //undefined behaviour if parameters don't make sense
void          BSEN_context_set_palette (BSEN_context* ctx, BSEN_rgb groups[]); //expected to be size 16
void		  BSEN_context_camera_pos  (BSEN_context* ctx, float x, float y, float z); //sets camera position
void		  BSEN_context_camera_angle(BSEN_context* ctx, float hangle, float vangle); //sets camera angle
void		  BSEN_context_camera_FOV  (BSEN_context* ctx, uint8_t FOV);
void		  BSEN_context_render      (BSEN_context* ctx);
void		  BSEN_context_clear_color (BSEN_context* ctx); //blank color will always be defined as group 0, intensity 0
void          BSEN_context_clear_flood (BSEN_context* ctx);
void		  BSEN_context_clear_depth (BSEN_context* ctx);

void BSEN_context_temp_push_wall  (BSEN_context* ctx, BSEN_wall* wall);
void BSEN_context_temp_push_flat  (BSEN_context* ctx, BSEN_flat* flat);
void BSEN_context_temp_push_model (BSEN_context* ctx, BSEN_model_instance* model);
void BSEN_context_temp_push_sprite(BSEN_context* ctx, BSEN_3D_sprite spr);

//looks for closest color in the palette
//set rotate to true to rotate the image 90 degrees to the right (do this for wall images basically)
BSEN_texture* BSEN_texture_load_png(BSEN_context* ctx, const char* filename, int rotate);

//file format for this will be the output from goxel's "text" option. Example:
/*
# Goxel 0.12.0
# One line per voxel
# X Y Z RRGGBB
-4 -16 0 8f563b
-3 -16 0 8f563b
-2 -16 0 8f563b
-1 -16 0 8f563b
(more voxels here...)
*/

BSEN_model* BSEN_model_load_text (BSEN_context* ctx, const char* filename);
void BSEN_model_free( BSEN_model* m );

void BSEN_context_draw_texture(BSEN_context* ctx, int x, int y, BSEN_texture* tex);

#endif
