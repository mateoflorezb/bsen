/*
	This is basically SDL's scancode.h file. Renamed macros for BSEN.
	Reason for this is that I don't want to be forced to include SDL
	in BSEN apps.

*/

#ifndef _BSEN_KEY_H
#define _BSEN_KEY_H

typedef enum
{
    BSEN_KEY_UNKNOWN = 0,

    BSEN_KEY_A = 4,
    BSEN_KEY_B = 5,
    BSEN_KEY_C = 6,
    BSEN_KEY_D = 7,
    BSEN_KEY_E = 8,
    BSEN_KEY_F = 9,
    BSEN_KEY_G = 10,
    BSEN_KEY_H = 11,
    BSEN_KEY_I = 12,
    BSEN_KEY_J = 13,
    BSEN_KEY_K = 14,
    BSEN_KEY_L = 15,
    BSEN_KEY_M = 16,
    BSEN_KEY_N = 17,
    BSEN_KEY_O = 18,
    BSEN_KEY_P = 19,
    BSEN_KEY_Q = 20,
    BSEN_KEY_R = 21,
    BSEN_KEY_S = 22,
    BSEN_KEY_T = 23,
    BSEN_KEY_U = 24,
    BSEN_KEY_V = 25,
    BSEN_KEY_W = 26,
    BSEN_KEY_X = 27,
    BSEN_KEY_Y = 28,
    BSEN_KEY_Z = 29,

    BSEN_KEY_1 = 30,
    BSEN_KEY_2 = 31,
    BSEN_KEY_3 = 32,
    BSEN_KEY_4 = 33,
    BSEN_KEY_5 = 34,
    BSEN_KEY_6 = 35,
    BSEN_KEY_7 = 36,
    BSEN_KEY_8 = 37,
    BSEN_KEY_9 = 38,
    BSEN_KEY_0 = 39,

    BSEN_KEY_RETURN = 40,
    BSEN_KEY_ESCAPE = 41,
    BSEN_KEY_BACKSPACE = 42,
    BSEN_KEY_TAB = 43,
    BSEN_KEY_SPACE = 44,

    BSEN_KEY_MINUS = 45,
    BSEN_KEY_EQUALS = 46,
    BSEN_KEY_LEFTBRACKET = 47,
    BSEN_KEY_RIGHTBRACKET = 48,
    BSEN_KEY_BACKSLASH = 49,
    BSEN_KEY_NONUSHASH = 50, 
    BSEN_KEY_SEMICOLON = 51,
    BSEN_KEY_APOSTROPHE = 52,
    BSEN_KEY_GRAVE = 53,
    BSEN_KEY_COMMA = 54,
    BSEN_KEY_PERIOD = 55,
    BSEN_KEY_SLASH = 56,

    BSEN_KEY_CAPSLOCK = 57,

    BSEN_KEY_F1 = 58,
    BSEN_KEY_F2 = 59,
    BSEN_KEY_F3 = 60,
    BSEN_KEY_F4 = 61,
    BSEN_KEY_F5 = 62,
    BSEN_KEY_F6 = 63,
    BSEN_KEY_F7 = 64,
    BSEN_KEY_F8 = 65,
    BSEN_KEY_F9 = 66,
    BSEN_KEY_F10 = 67,
    BSEN_KEY_F11 = 68,
    BSEN_KEY_F12 = 69,

    BSEN_KEY_PRINTSCREEN = 70,
    BSEN_KEY_SCROLLLOCK = 71,
    BSEN_KEY_PAUSE = 72,
    BSEN_KEY_INSERT = 73,
    BSEN_KEY_HOME = 74,
    BSEN_KEY_PAGEUP = 75,
    BSEN_KEY_DELETE = 76,
    BSEN_KEY_END = 77,
    BSEN_KEY_PAGEDOWN = 78,
    BSEN_KEY_RIGHT = 79,
    BSEN_KEY_LEFT = 80,
    BSEN_KEY_DOWN = 81,
    BSEN_KEY_UP = 82,

    BSEN_KEY_NUMLOCKCLEAR = 83,
    BSEN_KEY_KP_DIVIDE = 84,
    BSEN_KEY_KP_MULTIPLY = 85,
    BSEN_KEY_KP_MINUS = 86,
    BSEN_KEY_KP_PLUS = 87,
    BSEN_KEY_KP_ENTER = 88,
    BSEN_KEY_KP_1 = 89,
    BSEN_KEY_KP_2 = 90,
    BSEN_KEY_KP_3 = 91,
    BSEN_KEY_KP_4 = 92,
    BSEN_KEY_KP_5 = 93,
    BSEN_KEY_KP_6 = 94,
    BSEN_KEY_KP_7 = 95,
    BSEN_KEY_KP_8 = 96,
    BSEN_KEY_KP_9 = 97,
    BSEN_KEY_KP_0 = 98,
    BSEN_KEY_KP_PERIOD = 99,

    BSEN_KEY_NONUSBACKSLASH = 100,
    BSEN_KEY_APPLICATION = 101,
    BSEN_KEY_POWER = 102,
    BSEN_KEY_KP_EQUALS = 103,
    BSEN_KEY_F13 = 104,
    BSEN_KEY_F14 = 105,
    BSEN_KEY_F15 = 106,
    BSEN_KEY_F16 = 107,
    BSEN_KEY_F17 = 108,
    BSEN_KEY_F18 = 109,
    BSEN_KEY_F19 = 110,
    BSEN_KEY_F20 = 111,
    BSEN_KEY_F21 = 112,
    BSEN_KEY_F22 = 113,
    BSEN_KEY_F23 = 114,
    BSEN_KEY_F24 = 115,
    BSEN_KEY_EXECUTE = 116,
    BSEN_KEY_HELP = 117,
    BSEN_KEY_MENU = 118,
    BSEN_KEY_SELECT = 119,
    BSEN_KEY_STOP = 120,
    BSEN_KEY_AGAIN = 121,
    BSEN_KEY_UNDO = 122,
    BSEN_KEY_CUT = 123,
    BSEN_KEY_COPY = 124,
    BSEN_KEY_PASTE = 125,
    BSEN_KEY_FIND = 126,
    BSEN_KEY_MUTE = 127,
    BSEN_KEY_VOLUMEUP = 128,
    BSEN_KEY_VOLUMEDOWN = 129,
    BSEN_KEY_KP_COMMA = 133,
    BSEN_KEY_KP_EQUALSAS400 = 134,

    BSEN_KEY_INTERNATIONAL1 = 135,
    BSEN_KEY_INTERNATIONAL2 = 136,
    BSEN_KEY_INTERNATIONAL3 = 137,
    BSEN_KEY_INTERNATIONAL4 = 138,
    BSEN_KEY_INTERNATIONAL5 = 139,
    BSEN_KEY_INTERNATIONAL6 = 140,
    BSEN_KEY_INTERNATIONAL7 = 141,
    BSEN_KEY_INTERNATIONAL8 = 142,
    BSEN_KEY_INTERNATIONAL9 = 143,
    BSEN_KEY_LANG1 = 144,
    BSEN_KEY_LANG2 = 145, 
    BSEN_KEY_LANG3 = 146,
    BSEN_KEY_LANG4 = 147, 
    BSEN_KEY_LANG5 = 148, 
    BSEN_KEY_LANG6 = 149,
    BSEN_KEY_LANG7 = 150,
    BSEN_KEY_LANG8 = 151, 
    BSEN_KEY_LANG9 = 152,

    BSEN_KEY_ALTERASE = 153,
    BSEN_KEY_SYSREQ = 154,
    BSEN_KEY_CANCEL = 155,
    BSEN_KEY_CLEAR = 156,
    BSEN_KEY_PRIOR = 157,
    BSEN_KEY_RETURN2 = 158,
    BSEN_KEY_SEPARATOR = 159,
    BSEN_KEY_OUT = 160,
    BSEN_KEY_OPER = 161,
    BSEN_KEY_CLEARAGAIN = 162,
    BSEN_KEY_CRSEL = 163,
    BSEN_KEY_EXSEL = 164,

    BSEN_KEY_KP_00 = 176,
    BSEN_KEY_KP_000 = 177,
    BSEN_KEY_THOUSANDSSEPARATOR = 178,
    BSEN_KEY_DECIMALSEPARATOR = 179,
    BSEN_KEY_CURRENCYUNIT = 180,
    BSEN_KEY_CURRENCYSUBUNIT = 181,
    BSEN_KEY_KP_LEFTPAREN = 182,
    BSEN_KEY_KP_RIGHTPAREN = 183,
    BSEN_KEY_KP_LEFTBRACE = 184,
    BSEN_KEY_KP_RIGHTBRACE = 185,
    BSEN_KEY_KP_TAB = 186,
    BSEN_KEY_KP_BACKSPACE = 187,
    BSEN_KEY_KP_A = 188,
    BSEN_KEY_KP_B = 189,
    BSEN_KEY_KP_C = 190,
    BSEN_KEY_KP_D = 191,
    BSEN_KEY_KP_E = 192,
    BSEN_KEY_KP_F = 193,
    BSEN_KEY_KP_XOR = 194,
    BSEN_KEY_KP_POWER = 195,
    BSEN_KEY_KP_PERCENT = 196,
    BSEN_KEY_KP_LESS = 197,
    BSEN_KEY_KP_GREATER = 198,
    BSEN_KEY_KP_AMPERSAND = 199,
    BSEN_KEY_KP_DBLAMPERSAND = 200,
    BSEN_KEY_KP_VERTICALBAR = 201,
    BSEN_KEY_KP_DBLVERTICALBAR = 202,
    BSEN_KEY_KP_COLON = 203,
    BSEN_KEY_KP_HASH = 204,
    BSEN_KEY_KP_SPACE = 205,
    BSEN_KEY_KP_AT = 206,
    BSEN_KEY_KP_EXCLAM = 207,
    BSEN_KEY_KP_MEMSTORE = 208,
    BSEN_KEY_KP_MEMRECALL = 209,
    BSEN_KEY_KP_MEMCLEAR = 210,
    BSEN_KEY_KP_MEMADD = 211,
    BSEN_KEY_KP_MEMSUBTRACT = 212,
    BSEN_KEY_KP_MEMMULTIPLY = 213,
    BSEN_KEY_KP_MEMDIVIDE = 214,
    BSEN_KEY_KP_PLUSMINUS = 215,
    BSEN_KEY_KP_CLEAR = 216,
    BSEN_KEY_KP_CLEARENTRY = 217,
    BSEN_KEY_KP_BINARY = 218,
    BSEN_KEY_KP_OCTAL = 219,
    BSEN_KEY_KP_DECIMAL = 220,
    BSEN_KEY_KP_HEXADECIMAL = 221,

    BSEN_KEY_LCTRL = 224,
    BSEN_KEY_LSHIFT = 225,
    BSEN_KEY_LALT = 226,
    BSEN_KEY_LGUI = 227,
    BSEN_KEY_RCTRL = 228,
    BSEN_KEY_RSHIFT = 229,
    BSEN_KEY_RALT = 230, 
    BSEN_KEY_RGUI = 231,

    BSEN_KEY_MODE = 257,

    BSEN_KEY_AUDIONEXT = 258,
    BSEN_KEY_AUDIOPREV = 259,
    BSEN_KEY_AUDIOSTOP = 260,
    BSEN_KEY_AUDIOPLAY = 261,
    BSEN_KEY_AUDIOMUTE = 262,
    BSEN_KEY_MEDIASELECT = 263,
    BSEN_KEY_WWW = 264,
    BSEN_KEY_MAIL = 265,
    BSEN_KEY_CALCULATOR = 266,
    BSEN_KEY_COMPUTER = 267,
    BSEN_KEY_AC_SEARCH = 268,
    BSEN_KEY_AC_HOME = 269,
    BSEN_KEY_AC_BACK = 270,
    BSEN_KEY_AC_FORWARD = 271,
    BSEN_KEY_AC_STOP = 272,
    BSEN_KEY_AC_REFRESH = 273,
    BSEN_KEY_AC_BOOKMARKS = 274,

    BSEN_KEY_BRIGHTNESSDOWN = 275,
    BSEN_KEY_BRIGHTNESSUP = 276,
    BSEN_KEY_DISPLAYSWITCH = 277,
    BSEN_KEY_KBDILLUMTOGGLE = 278,
    BSEN_KEY_KBDILLUMDOWN = 279,
    BSEN_KEY_KBDILLUMUP = 280,
    BSEN_KEY_EJECT = 281,
    BSEN_KEY_SLEEP = 282,

    BSEN_KEY_APP1 = 283,
    BSEN_KEY_APP2 = 284,

    BSEN_KEY_AUDIOREWIND = 285,
    BSEN_KEY_AUDIOFASTFORWARD = 286,

    BSEN_NUM_KEYS = 512
	
} __BSEN_KEY__;

#endif