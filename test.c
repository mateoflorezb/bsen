#define _USE_MATH_DEFINES

#include "BSEN_app.h"
#include "BSEN_app_keycode.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

//#define SCREEN_W (288)
//#define SCREEN_H (180)
#define SCREEN_W (320)
#define SCREEN_H (200)
#define WINDOW_INTEGER_SCALE 2
#define MS_PER_FRAME 16


static inline int iclamp(int v, int min, int max) {
	if (v < min) return min;
	if (v > max) return max;
	return v;
}

static void construct_flat(BSEN_app* app, BSEN_float2* points, int npoints, BSEN_texture* tx, float z, uint8_t type) {
    BSEN_flat* newflat = malloc(sizeof(BSEN_flat));
    newflat->shape.nsides = npoints;
    newflat->tx = tx;
    newflat->z = z;
    newflat->type = type;
    newflat->dynamic_angle = 0;
    newflat->dynamic_plane = 0;
    newflat->dynamic_xoffset = 0;
    newflat->dynamic_yoffset = 0;

    newflat->shape.side = malloc( npoints * sizeof(BSEN_float2_line) );

    for (int k = 0; k < npoints; k++) {
        memcpy( &newflat->shape.side[k].point[0], &points[k], sizeof(BSEN_float2) );
        memcpy( &newflat->shape.side[k].point[1], &points[(k+1) % npoints], sizeof(BSEN_float2) );
    }

    BSEN_context_temp_push_flat( app->ctx, newflat );

}

static void insert_box(BSEN_app* app, BSEN_wall* walls, BSEN_texture* tx, int* ida, int x, int y, int h) {

    int s = 64;
    //int h = 300;
    int id = *ida;

    walls[id] = (BSEN_wall){
        .base_z = 0,
        .height = h,
        .line.point[0] = { x, y },
        .line.point[1] = { x, y + s },
        .draw_from_behind = 1,
        .tx = tx,
        .point_texture_y[0] = 0,
        .point_texture_y[1] = 64,
        .base_texture_x = 0,
        .top_texture_x = h
    };

    BSEN_context_temp_push_wall( app->ctx, &walls[id] );

    id++;

    walls[id] = (BSEN_wall){
        .base_z = 0,
        .height = h,
        .line.point[1] = { x + s, y },
        .line.point[0] = { x + s, y + s },
        .draw_from_behind = 1,
        .tx = tx,
        .point_texture_y[0] = 0,
        .point_texture_y[1] = 64,
        .base_texture_x = 0,
        .top_texture_x = h
    };

    BSEN_context_temp_push_wall( app->ctx, &walls[id] );

    id++;

    walls[id] = (BSEN_wall){
        .base_z = 0,
        .height = h,
        .line.point[1] = { x, y },
        .line.point[0] = { x + s, y },
        .draw_from_behind = 1,
        .tx = tx,
        .point_texture_y[0] = 0,
        .point_texture_y[1] = 64,
        .base_texture_x = 0,
        .top_texture_x = h
    };

    BSEN_context_temp_push_wall( app->ctx, &walls[id] );

    id++;

    walls[id] = (BSEN_wall){
        .base_z = 0,
        .height = h,
        .line.point[0] = { x, y + s },
        .line.point[1] = { x + s, y + s },
        .draw_from_behind = 1,
        .tx = tx,
        .point_texture_y[0] = 0,
        .point_texture_y[1] = 64,
        .base_texture_x = 0,
        .top_texture_x = h
    };

    BSEN_context_temp_push_wall( app->ctx, &walls[id] );

    id++;

    *ida = id;

}

int main() {

    BSEN_set_high_priority();

	BSEN_app* app = BSEN_app_create("Test", SCREEN_W, SCREEN_H, 3);

	BSEN_app_set_window_size(app, SCREEN_W*WINDOW_INTEGER_SCALE, SCREEN_H*WINDOW_INTEGER_SCALE);

    /*
	BSEN_rgb colormap[16];
	colormap[0]  = (BSEN_rgb){.r=255, .g=255, .b=255 }; //greyscale
	colormap[1]  = (BSEN_rgb){.r=213, .g=235, .b=233 }; //pale blue
	colormap[2]  = (BSEN_rgb){.r=130, .g=78,  .b=38  }; //dirt
	colormap[3]  = (BSEN_rgb){.r=119, .g=95,  .b=75  }; //brown1
	colormap[4]  = (BSEN_rgb){.r=135, .g=87,  .b=51  }; //brown2
	colormap[5]  = (BSEN_rgb){.r=97,  .g=112, .b=98  }; //concrete1
	colormap[6]  = (BSEN_rgb){.r=90,  .g=90,  .b=90  }; //concrete2
	colormap[7]  = (BSEN_rgb){.r=94,  .g=108, .b=119 }; //concrete3
	colormap[8]  = (BSEN_rgb){.r=255, .g=251, .b=122 }; //sand
	colormap[9]  = (BSEN_rgb){.r=170, .g=243, .b=243 }; //sky
	colormap[10] = (BSEN_rgb){.r=112, .g=215, .b=255 }; //water
	colormap[11] = (BSEN_rgb){.r=0,   .g=75,  .b=255 }; //water2
	colormap[12] = (BSEN_rgb){.r=95,  .g=255, .b=51  }; //vegetation1
	colormap[13] = (BSEN_rgb){.r=120, .g=220, .b=15  }; //vegetation2
	colormap[14] = (BSEN_rgb){.r=108, .g=128, .b=80  }; //vegetation3
	colormap[15] = (BSEN_rgb){.r=255, .g=0,   .b=0   }; //red
	*/


	BSEN_rgb colormap[16];
	colormap[0]  = (BSEN_rgb){.r=255, .g=255, .b=255 }; //greyscale
	colormap[1]  = (BSEN_rgb){.r=213, .g=235, .b=233 }; //pale blue
	colormap[2]  = (BSEN_rgb){.r=130, .g=78,  .b=38  }; //dirt
	colormap[3]  = (BSEN_rgb){.r=165, .g=110, .b=78  }; //rust
	colormap[4]  = (BSEN_rgb){.r=210, .g=168, .b=147 }; //wood1
	colormap[5]  = (BSEN_rgb){.r=97,  .g=112, .b=98  }; //concrete1
	colormap[6]  = (BSEN_rgb){.r=90,  .g=90,  .b=90  }; //concrete2
	colormap[7]  = (BSEN_rgb){.r=94,  .g=108, .b=119 }; //concrete3
	colormap[8]  = (BSEN_rgb){.r=255, .g=251, .b=122 }; //sand
	colormap[9]  = (BSEN_rgb){.r=170, .g=243, .b=243 }; //sky
	colormap[10] = (BSEN_rgb){.r=112, .g=215, .b=255 }; //water
	colormap[11] = (BSEN_rgb){.r=0,   .g=75,  .b=255 }; //water2
	colormap[12] = (BSEN_rgb){.r=95,  .g=255, .b=51  }; //vegetation1
	colormap[13] = (BSEN_rgb){.r=120, .g=220, .b=15  }; //vegetation2
	colormap[14] = (BSEN_rgb){.r=108, .g=128, .b=80  }; //vegetation3
	colormap[15] = (BSEN_rgb){.r=255, .g=0,   .b=0   }; //red


	BSEN_context_set_palette(app->ctx, colormap);

	BSEN_texture* textures[6];
	textures[0] = BSEN_texture_load_png(app->ctx, "wood.png", 0);
	textures[1] = BSEN_texture_load_png(app->ctx, "brick1.png", 1);
	textures[2] = BSEN_texture_load_png(app->ctx, "brick2.png", 1);
	textures[3] = BSEN_texture_load_png(app->ctx, "rust.png", 0);
	textures[4] = BSEN_texture_load_png(app->ctx, "leaves.png", 1);
	textures[5] = BSEN_texture_load_png(app->ctx, "test.png", 1);

	BSEN_model* voxel = BSEN_model_load_text ( app->ctx, "voxel.txt" );

	BSEN_wall* walls = malloc(1024 * 200);

	int id = 0;

    /*
	for (int j = 0; j < 10; j++)
	for (int k = 0; k < 10; k++) {

        walls[id] = (BSEN_wall){
            .base_z = 0,
            .height = 30,
            .line.point[0] = { 20 + 30*k, -10 + 30*j},
            .line.point[1] = { 20 + 30*k,  10 + 30*j},
            .draw_from_behind = 1,
            .tx = textures[id % 4],
            .point_texture_y[0] = 0,
            .point_texture_y[1] = 64*2,
            .base_texture_x = 0,
            .top_texture_x = 64
        };

        BSEN_context_temp_push_wall( app->ctx, &walls[id] );

        id++;
	}
	*/

    if (1) {
        for (int j = 0; j < 10; j++)
        for (int k = 0; k < 10; k++) {

            insert_box(app, walls, textures[(k + j) % 4], &id, 20 + 100*k, 20 + 100*j, 20 + id*3);

        }

        insert_box(app, walls, textures[5], &id, 40, -400, 64);
    }

	BSEN_float2 testFloorPoints[5] = {
        {0,0},
        {0,200},
        {200,200},
        {400,100},
        {200,0}
	};

    for (int k = 0; k < 10; k++)
	construct_flat(app, testFloorPoints, 5, textures[(k+2) % 5], - k * 100, BSEN_FLAT_TYPE_BOTH );

	BSEN_wall longWall = (BSEN_wall) {
        .base_z = 0,
        .height = 450,
        .line.point[0] = { -40,  200},
        .line.point[1] = { -40, -200},
        .draw_from_behind = 1,
        .tx = textures[0],
        .point_texture_y[0] = 0,
        .point_texture_y[1] = 400,
        .base_texture_x = 0,
        .top_texture_x = 450
    };

    BSEN_context_temp_push_wall(app->ctx, &longWall);

    BSEN_model_instance mInstance[100];
    for (int k = 0; k < 0; k++) {

        mInstance[k].model = voxel;
        mInstance[k].pos = (BSEN_float3){10 + 64 * (k / 10), (64 * (k % 10) ), -4};
        mInstance[k].hangle = 0;
        mInstance[k].scale = 1;

        BSEN_context_temp_push_model( app->ctx, &mInstance[k] );

    }

    BSEN_texture* spr_tx = BSEN_texture_load_png( app->ctx, "sprite.png", 1);

    BSEN_3D_sprite sp3D;
    sp3D.pos = (BSEN_float3){ 15, 30, 0 };
    sp3D.sprite = spr_tx;
    sp3D.zoffset = 0;

    BSEN_context_temp_push_sprite( app->ctx, sp3D );

	float x = 0, y = 0, z = -10, ha = 0, va = 0;
	char prevHeld = 0;

	char mouseMove = 0;

	int fpCap = 0;

	int time_notification_delay = 10;

	while (app->quit == 0) {
		int64_t tstart = BSEN_get_ms();
		//fpCap = (fpCap + 1) % 2;
		fpCap = 1;
		BSEN_app_refresh_inputs(app);

		if (app->input.mouse_held[0] && !prevHeld) {
			mouseMove = ~mouseMove;
			BSEN_set_mouse_mode(BSEN_MOUSE_MODE_SWAP);
		}

		if (mouseMove != 0) {
			ha -= ((float) app->input.mouse_xrel ) / 80.0;
			va -= ((float) app->input.mouse_yrel ) / 120.0;
		}

		va = fmin(fmax(va, -1.0), 1.0);
        //va = 0;

		float sp = 2;//0.8;

		if (BSEN_app_check_keyboard(app, BSEN_KEY_W)) {
			x += sp * cos(ha);
			y -= sp * sin(ha);
		}

		if (BSEN_app_check_keyboard(app, BSEN_KEY_S)) {
			x -= sp * cos(ha);
			y += sp * sin(ha);
		}

		if (BSEN_app_check_keyboard(app, BSEN_KEY_A)) {
			x += sp * cos(ha + (M_PI/2.0) );
			y -= sp * sin(ha + (M_PI/2.0) );
		}

		if (BSEN_app_check_keyboard(app, BSEN_KEY_D)) {
			x -= sp * cos(ha + (M_PI/2.0) );
			y += sp * sin(ha + (M_PI/2.0) );
		}

		if (BSEN_app_check_keyboard(app, BSEN_KEY_SPACE)) {
			z -= sp;
		}

		if (BSEN_app_check_keyboard(app, BSEN_KEY_LCTRL)) {
			z += sp;
		}

		if (BSEN_app_check_keyboard(app, BSEN_KEY_LEFT)) {
			app->ctx->light.diminished_range -= 5;
		}

		if (BSEN_app_check_keyboard(app, BSEN_KEY_RIGHT)) {
			app->ctx->light.diminished_range += 5;
		}

		if (BSEN_app_check_keyboard(app, BSEN_KEY_B)) {

			printf("Breaking!\n");
		}

		app->ctx->light.diminished_range = fmax(app->ctx->light.diminished_range, 1);

		if (BSEN_app_check_keyboard(app, BSEN_KEY_R)) {
			x = 0;
			y = 0;
			z = 0;
			ha = 0;
			va = 0;
		}

		if (BSEN_app_check_keyboard(app, BSEN_KEY_BACKSPACE)) {
			app->quit = 1;
		}

		////// TEMP TEMP TEMP

		//app->ctx->light.directional_angle = M_PI * (float)( (BSEN_get_ms() / 10) % 360) / 180.0;

        if (1)
		for (int f = 0; f < app->ctx->temp_scene.nmodels; f++) {
            app->ctx->temp_scene.models[f]->hangle = (float)f + ((float)BSEN_get_ms()) / 1000.0;
		}

        if (0)
		for (int f = 0; f < app->ctx->temp_scene.nflats; f++) {

            BSEN_flat* flt = app->ctx->temp_scene.flats[f];

            float theta = ((float)BSEN_get_ms()) / 300.0;

            float uv = (BSEN_get_ms() / 32) % 64;

            switch (f % 4) {

                case 0:
                flt->dynamic_angle = theta;
                break;

                case 1:
                flt->dynamic_xoffset = uv;
                break;

                case 2:
                flt->dynamic_yoffset = uv;
                break;

                case 3:
                flt->dynamic_angle = 0.6;
                flt->dynamic_xoffset = uv;
                flt->dynamic_yoffset = uv;
                break;

                default: break;

            }

		}


		BSEN_context_camera_pos(app->ctx, x,y,z); //sets camera position
		BSEN_context_camera_angle(app->ctx, ha, va); //sets camera angle

		clock_t preRender = clock();
		clock_t postRender;

		if (fpCap) {
            BSEN_context_render(app->ctx);
            postRender = clock();
            BSEN_app_refresh_window(app);
		}

        if ( time_notification_delay > 0 ) {
            time_notification_delay--;
        }
        else {
            time_notification_delay = 10;
            //printf("Took %d clocks\n", (int) (postRender - preRender) );
        }

		prevHeld = app->input.mouse_held[0];

		int64_t tend = BSEN_get_ms();
		int64_t tsleep = MS_PER_FRAME - (tend - tstart);
		if (tsleep > 0) BSEN_delay(tsleep);
	}

	//scanf("*");
	return 0;
}
